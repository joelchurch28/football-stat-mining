class PassStatsPerGame:
    def __init__(self, name, yds, att, cmp, td, int, yds_per_att, cmp_pct, rating, wins, losses, ties):

        games_played = wins + losses + ties

        self.name = name
        self.pass_yards_per_game = round(yds / games_played, 1)
        self.pass_att_per_game = round(att/ games_played, 1)
        self.cmp_per_game = round(cmp / games_played, 1)
        self.pass_td_per_game = round(td / games_played, 1)
        self.int_per_game = round(int / games_played, 1)

        self.pass_yards_per_attempt = yds_per_att
        self.completion_percentage = cmp_pct
        self.passer_rating = rating
