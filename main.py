from bs4 import BeautifulSoup
import requests
from Players import player_class, quarterback_class
import re
import time
from bs4 import BeautifulSoup
import lxml
import requests
from Players.quarterback_class import Quarterback
from PyQt5.QtWidgets import QApplication
from SearchFunctions.passingfunctions import *

from PyQt5.QtWidgets import QApplication
from GUI.main_window import MainWindow

from GUI.singleton_main_window import SingletonMainWindow
import sys


url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"

# Open the NFL passing stats page and load it into a readable data structure
r = requests.get(url)
soup = BeautifulSoup(r.text, "lxml")

# Dictionary that stores years as keys and subsequent urls as the values
passing_year_dict = {}

# Locate the dropdown list of years with passing stats and put data into dictionary
list_years = soup.find("select", class_="d3-o-dropdown")
for i in list_years:
    year = i.text.strip().replace('\n', '')
    year_url = str(i)[15:82] if len(str(i)) == 97 else str(i)[27:94]
    if len(year) > 0:
        passing_year_dict[year] = year_url

# def window():
#     app = QApplication(sys.argv)
#     global main_window
#     main_window = MainWindow()
#     main_window.show()
#     sys.exit(app.exec_())

app = QApplication(sys.argv)
#global main_window
#main_window = MainWindow()

if __name__ == "__main__":
    # window()

    # qb1 = input("First QB to compare: ")
    # qb2 = input("Second QB to compare: ")
    # test1, test2 = process_single_passer_year(qb1, 2023), process_single_passer_year(qb2, 2022)
    # #test1, test2 = process_passer_career(qb1), process_passer_career(qb2)
    # # test1, test2 = qb_compare(qb1, qb2, "2022")
    # display_qb_comparison(test1, test2)

    main_window = SingletonMainWindow.get_instance()
    main_window.show()
    sys.exit(app.exec_())

    start = time.time()
    # qb_list = process_passing_current_without_teams()
    # qb_list = process_passing_year(2021)
    # for i in range(len(qb_list)):
    #     print(str(i) + ":", qb_list[i].name, qb_list[i].yds_per_att)
    # dak_stats = process_passer_career("Philip Rivers")
    # print(dak_stats.pass_yds, dak_stats.rating)
    # for i in qb_list:
    #     print(i.name, i.pass_yds)
    # test1, test2 = qb_compare("Dak Prescott", "Lamar Jackson", 2023)


    #print("Done in", time.time() - start, "seconds")

    #window()

    #Initial commit to new repository! (Hello Gitlab)
