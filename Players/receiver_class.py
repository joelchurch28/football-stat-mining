from Players import player_class


class Receiver(player_class.Player):
    def __init__(self, name, year, team, number, yds, td, twenty_plus, forty_plus, lng, rec_first, rec_first_pct,
                 rec_fum, rec_yac, tgt):
        super().__init__(name, year, team, number)
        self.yds = yds
        self.td = td
        self.twenty_plus = twenty_plus
        self.forty_plus = forty_plus
        self.lng = lng
        self.rec_first = rec_first
        self.rec_first_pct = rec_first_pct
        self.rec_fum = rec_fum
        self.rec_yac = rec_yac
        self.tgt = tgt

