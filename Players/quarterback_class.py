from Players import player_class


class Quarterback(player_class.Player):
    def __init__(self, name="None", year="None", team="None", number="None", pass_yds=0, yds_per_att=0, att=0, cmp=0,
                 cmp_pct=0, td=0, ints=0, rating=0, firsts=0, first_pct=0, twenty_plus=0, forty_plus=0, lng=0, sck=0,
                 sck_yds=0):
        super().__init__(name, year, team, number)
        self.pass_yds = int(pass_yds)
        self.yds_per_att = float(yds_per_att)
        self.att = int(att)
        self.cmp = int(cmp)
        self.cmp_pct = float(cmp_pct)
        self.td = int(td)
        self.ints = int(ints)
        self.rating = float(rating)
        self.firsts = int(firsts)
        self.first_pct = float(first_pct)
        self.twenty_plus = int(twenty_plus)
        self.forty_plus = int(forty_plus)
        self.lng = int(lng)
        self.sck = int(sck)
        self.sck_yds = int(sck_yds)
