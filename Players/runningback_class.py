from Players import player_class


class Runningback(player_class.Player):
    def __init__(self, name, year, team, number, rush_yds, att, td, twenty_plus, forty_plus, lng,
                 rush_first, rush_first_pct, rush_fum):
        super().__init__(name, year, team, number)
        self.rush_yds = rush_yds
        self.att = att
        self.td = td
        self.twenty_plus = twenty_plus
        self.forty_plus = forty_plus
        self.lng = lng
        self.rush_first = rush_first
        self.rush_first_pct = rush_first_pct
        self.rush_fum = rush_fum

