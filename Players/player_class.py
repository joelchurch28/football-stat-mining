class Player:
    def __init__(self, name, year, team, number):
        self.id = str.upper(name.split(" ")[-1] + team.split(" ")[-1] + number + str(year))
        self.name = name
        self.year = year
        self.team = team
        self.number = number