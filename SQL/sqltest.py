import mysql.connector
import re
import time
from bs4 import BeautifulSoup
import lxml
import requests

db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="ds373737",
    database="FootballStats"
)

import random

mycursor = db.cursor()

mycursor.execute("SHOW TABLES")

tables = mycursor.fetchall()


# for i in tables:
#     print(i)

def all_quarterbacks_since_1970():
    """Draws names from pro football reference to retrieve a list of every quarterback to play in the NFl after 1970
    and their draft year

    :return: List of strings -> list
    """

    qb_names = []

    qb_names_link = "https://www.pro-football-reference.com/players/qbindex.htm"
    qb_names_r = requests.get(qb_names_link)
    qb_names_page = BeautifulSoup(qb_names_r.text, "lxml")
    qb_table_index = qb_names_page.find("div", class_="index")
    qb_table_rows = qb_table_index.find_all("tr")

    for row in qb_table_rows:
        if '-' in row.text and int(row.text[-8:-4]) > 1970:
            qb_names.append(row.text.split('-')[0][0:-2])
        elif 'QB' in row.text and '+' not in row.text and int(row.text[-8:-4]) > 1970:
            qb_names.append(row.text[0:row.text.index("QB")])
        elif 'QB' in row.text and '+' in row.text and int(row.text[-8:-4]) > 1970:
            qb_names.append(row.text[0:row.text.index("+")])

    return qb_names


def all_qbs_and_draft_years():
    qb_names = {}

    qb_names_link = "https://www.pro-football-reference.com/players/qbindex.htm"
    qb_names_r = requests.get(qb_names_link)
    qb_names_page = BeautifulSoup(qb_names_r.text, "lxml")
    qb_table_index = qb_names_page.find("div", class_="index")
    qb_table_rows = qb_table_index.find_all("tr")

    for row in qb_table_rows:
        # print(row.text)
        name = row.text.split("QB")[0]
        year = row.text[-8:-4]

        # if '-' in row.text and int(row.text[-8:-4]) > 1970:
        #     qb_names[row.text.split('-')[0][0:-2].strip().replace('+', '')] = row.text[-8:-4]
        # elif 'QB' in row.text and '+' not in row.text and int(row.text[-8:-4]) > 1970:
        #     qb_names[row.text[0:row.text.index("QB")]] = row.text[-8:-4]
        #     # if '+' in row.text[0:row.text.index("QB")]:
        #     #     print(row.text[0:row.text.index("QB")], row.text[-8:-4])
        # elif 'QB' in row.text and '+' in row.text and int(row.text[-8:-4]) > 1970:
        #     qb_names[row.text[0:row.text.index("+")]] = row.text[-8:-4]
        #
        #     # if '+' in row.text[0:row.text.index("+")]:
        #     #     print(row.text[0:row.text.index("+")], row.text[-8:-4])

        if '+' in name:
            name = name.strip().replace('+', '')
        if '-' in name:
            chars_erased = -1
            while (name[chars_erased] == '-' or name[chars_erased].isupper()):
                chars_erased -= 1
            name = name[:chars_erased + 1]
        if ' ' in name and int(year) >= 1970:
            qb_names[name] = year

    return qb_names


# def insert_passer_statlines(passer_name, passer_draft_year, cursor, database):
#     """Inserts data for all passers into Player and PasserStatline tables of the database
#
#     :param passer_name: -> str
#     :param passer_draft_year: -> str
#     :param cursor: a cursor object connected to a valid SQL database
#     :param database: the database to add tables to
#     :return:
#     """
#
#     original_name = passer_name
#
#     count = 0
#     passer_link_number = "00"
#     website_player_name = ""
#     website_draft_year = ""
#     receiving_included = False
#     tackles_included = False
#     punting_included = False
#     player_has_playoff_career = False
#     wins = 0
#     losses = 0
#     ties = 0
#     include_periods = False
#
#     if "'" in passer_name:
#         passer_name = passer_name.strip().replace("'", '')
#     if "." in passer_name and int(passer_draft_year) <= 1974:
#         passer_name = passer_name.strip().replace(".", '')
#
#     # Since the pro football reference player url includes the number in order of when they were drafted compared to
#     # players with similar names, we need to increment this number until their name and draft year match
#     while website_player_name != passer_name or website_draft_year != passer_draft_year:
#         if original_name == "P.J. Walker":
#             passer_gamelog_link = "https://www.pro-football-reference.com/players/W/WalkPh00/gamelog/"
#         if original_name == "Gino Torretta":
#             passer_gamelog_link = "https://www.pro-football-reference.com/players/T/ToreGi00/gamelog/"
#         elif len(passer_name.split(' ')) == 2:
#             formatted_passer_name = passer_name.split(' ')[1][:4] + passer_name.split(' ')[0][:2] + str(
#                 int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[1]) > 3 \
#                 else (passer_name.split(' ')[-1] + 'x' * (4 - len(passer_name.split(' ')[1])) + passer_name.split(' ')[
#                                                                                                     0][:2] +
#                       str(int(passer_link_number) + count).zfill(2))
#             passer_gamelog_link = f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[1][0]}/{formatted_passer_name}/gamelog/"
#         # If player has three or more names (i.e. Billy Joe Redneck)
#         else:
#             # The website seems to randomly switch between using the second and third name, so if it doesn't work after
#             # 20 tries with the third name, try the second
#             if count < 21 and not include_periods:
#                 # Formatted name will be RednBi00
#                 formatted_passer_name = passer_name.split(' ')[-1][:4] + passer_name.split(' ')[0][:2] + str(
#                     int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[-1]) > 3 else (
#                         passer_name.split(' ')[-1][:4] + passer_name.split(' ')[0][:(4 - len(passer_name.split(' ')[1]))] +
#                         passer_name.split(' ')[0][:2] +
#                         str(int(passer_link_number) + count).zfill(2))
#                 passer_gamelog_link = (f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[-1][0]}/"
#                                        f"{formatted_passer_name}/gamelog/")
#             else:
#                 # Formatted name will be JoeRBi00
#                 if not include_periods:
#                     count = 0
#                 include_periods = True
#                 formatted_passer_name = passer_name.split(' ')[1][:4] + passer_name.split(' ')[0][:2] + str(
#                     int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[1]) > 3 else (
#                         passer_name.split(' ')[1] + passer_name.split(' ')[-1][:(4 - len(passer_name.split(' ')[1]))] +
#                         passer_name.split(' ')[0][:2] + str(int(passer_link_number) + count).zfill(2))
#                 passer_gamelog_link = (f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[1][0]}/"
#                                        f"{formatted_passer_name}/gamelog/")
#         passer_gamelog_r = requests.get(passer_gamelog_link)
#         passer_gamelog_page = BeautifulSoup(passer_gamelog_r.text, "lxml")
#         # possible_error = passer_gamelog_page.find("div")
#         # for i in possible_error:
#         #     print(i.text)
#         print(passer_gamelog_link)
#         if count >= 10:
#             time.sleep(10)
#         passer_info = passer_gamelog_page.find("div", class_="players")
#         if passer_info is None:
#             if 3 > count < 19 and "." not in passer_name:
#                 count = 19
#             else:
#                 count += 1
#             continue
#         website_player_name_tag = passer_info.find("h1")
#         website_player_name = website_player_name_tag.text.strip().replace('\n', '').strip().replace("'", '')
#         website_draft_year_tag = passer_info.find(attrs={"data-tip": True})
#         if website_draft_year_tag is None:
#             count += 1
#             print("website draft year tag is none")
#             continue
#         website_draft_year = website_draft_year_tag.get("data-tip")[-4:] if '-' not in website_draft_year_tag.get(
#             "data-tip") else website_draft_year_tag.get("data-tip")[-9:-5]
#         count += 1
#
#
#     all_tables = passer_gamelog_page.find_all("div", class_="table_wrapper")
#     passer_all_stats = passer_gamelog_page.find("div", class_="table_wrapper")
#     passer_regular_season_table = passer_all_stats.find("tbody")
#     regular_season_games = passer_regular_season_table.find_all("td")
#
#     regular_season_header = passer_all_stats.find("thead")
#     regular_season_headers = regular_season_header.find_all("th")
#
#     # If there are 2 tables on the page, one of them is for playoffs, and we need to process it
#     if len(all_tables) == 2:
#         player_has_playoff_career = True
#
#     # Initialize required indexes to -1, so if they are not found in the statline, don't include them
#     season_index = -1
#     date_index = -1
#     week_index = -1
#     home_away_index = -1
#     opponent_index = -1
#     result_index = -1
#     cmp_index = -1
#     pass_att_index = -1
#     cmp_pct_index = -1
#     pass_yds_index = -1
#     pass_td_index = -1
#     int_index = -1
#     rating_index = -1
#     times_sacked_index = -1
#     sack_yds_index = -1
#     pass_yds_per_att_index = -1
#     rush_att_index = -1
#     rush_yds_index = -1
#     rush_yds_per_att_index = -1
#     rush_td_index = -1
#     fmb_index = -1
#     fmb_lost_index = -1
#
#     statline_started = False
#     stat_index = 0
#     for header in regular_season_headers:
#         if "year_id" == header.get("data-stat"):
#             statline_started = True
#         if statline_started:
#             stat_index += 1
#             # print(header.get("data-stat"))
#             if "year_id" == header.get("data-stat"):
#                 season_index = stat_index
#             if "game_date" == header.get("data-stat"):
#                 date_index = stat_index
#             if "week_num" == header.get("data-stat"):
#                 week_index = stat_index
#             if "game_location" == header.get("data-stat"):
#                 home_away_index = stat_index
#             if "opp" == header.get("data-stat"):
#                 opponent_index = stat_index
#             if "game_result" == header.get("data-stat"):
#                 result_index = stat_index
#             if "pass_cmp" == header.get("data-stat"):
#                 cmp_index = stat_index
#             if "pass_att" == header.get("data-stat"):
#                 pass_att_index = stat_index
#             if "pass_cmp_perc" == header.get("data-stat"):
#                 cmp_pct_index = stat_index
#             if "pass_yds" == header.get("data-stat"):
#                 pass_yds_index = stat_index
#             if "pass_td" == header.get("data-stat"):
#                 pass_td_index = stat_index
#             if "pass_int" == header.get("data-stat"):
#                 int_index = stat_index
#             if "pass_rating" == header.get("data-stat"):
#                 rating_index = stat_index
#             if "pass_sacked" == header.get("data-stat"):
#                 times_sacked_index = stat_index
#             if "pass_sacked_yds" == header.get("data-stat"):
#                 sack_yds_index = stat_index
#             if "pass_yds_per_att" == header.get("data-stat"):
#                 pass_yds_per_att_index = stat_index
#             if "rush_att" == header.get("data-stat"):
#                 rush_att_index = stat_index
#             if "rush_yds" == header.get("data-stat"):
#                 rush_yds_index = stat_index
#             if "rush_yds_per_att" == header.get("data-stat"):
#                 rush_yds_per_att_index = stat_index
#             if "rush_td" == header.get("data-stat"):
#                 rush_td_index = stat_index
#             if "fumbles" == header.get("data-stat"):
#                 fmb_index = stat_index
#             if "fumbles_lost" == header.get("data-stat"):
#                 fmb_lost_index = stat_index
#
#     statline_length = stat_index
#
#     all_indexes = [season_index, date_index, week_index, home_away_index, opponent_index, result_index, cmp_index,
#                    pass_att_index, cmp_pct_index, pass_yds_index, pass_td_index, int_index, rating_index,
#                    times_sacked_index, sack_yds_index, pass_yds_per_att_index, rush_att_index, rush_yds_index,
#                    rush_yds_per_att_index, rush_td_index, fmb_index, fmb_lost_index]
#
#     # all_indexes_names = ["season_index", "date_index", "week_index", "home_away_index", "opponent_index", "result_index", "cmp_index",
#     #                "pass_att_index", "cmp_pct_index", "pass_yds_index", "pass_td_index", "int_index", "rating_index",
#     #                "times_sacked_index", "sack_yds_index", "pass_yds_per_att_index", "rush_att_index", "rush_yds_index",
#     #                "rush_yds_per_att_index", "rush_td_index", "fmb_index", "fmb_lost_index"]
#     #
#     # for i in range(len(all_indexes_names)):
#     #     print(all_indexes_names[i] + ':', all_indexes[i])
#
#     regular_season_statlines = []
#
#     # List of strings representing a regular season game's statline, containing (in this order):
#     # season, date, subseason, week, home/away, opponent, result, completions, pass attempts, completion %, pass yards,
#     # pass touchdowns, interceptions, passer rating, times sacked, sack yards, pass yards/att, rush attempts,
#     # rush yards, rush yards/attempt, rush touchdowns, fumbles, fumbles lost
#     reg_game_statline = []
#     count = 0
#
#     cursor.execute(f"""
#             INSERT INTO Player (name, position, wins, losses, ties)
#             VALUES ("{original_name}", 'Passer', {wins}, {losses}, {ties});
#             """)
#
#     new_player_id = cursor.lastrowid
#     # print("ID:", new_player_id)
#
#     for game_stat in regular_season_games:
#         stat = game_stat.text
#         count += 1
#         # print(f"{count}: {stat}")
#
#         # If player was inactive or injured, skip to next statline
#         # if ("Inactive" in stat or "Injured" in stat or "Did" in stat
#         #         or "Suspended" in stat or "COVID" in stat or "Exempt" in stat):
#         if game_stat.get("data-stat") == "reason":
#             count = statline_length
#             reg_game_statline = []
#         # Skip unused stats
#         if count not in all_indexes and count != statline_length:
#             continue
#         # Season
#         if count == season_index or (season_index == -1 and len(reg_game_statline) == 0):
#             reg_game_statline.append(int(stat) if season_index != -1 else 0)
#         # Date
#         if count == date_index or (date_index == -1 and len(reg_game_statline) == 1):
#             reg_game_statline.append(stat if date_index != -1 else "0000-00-00")
#             reg_game_statline.append('reg')
#         # Week
#         if count == week_index or (week_index == -1 and len(reg_game_statline) == 3):
#             reg_game_statline.append(int(stat) if week_index != -1 else 0)
#         # Home/Away
#         if count == home_away_index or (home_away_index == -1 and len(reg_game_statline) == 4):
#             reg_game_statline.append(("away" if '@' in stat else "home") if home_away_index != -1 else "unknown")
#         # Opponent
#         if count == opponent_index or (opponent_index == -1 and len(reg_game_statline) == 5):
#             reg_game_statline.append(stat if opponent_index != -1 else "unknown")
#         # Result
#         if count == result_index or (result_index == -1 and len(reg_game_statline) == 6):
#             reg_game_statline.append(stat if result_index != -1 else "unknown")
#             if 'W' in stat:
#                 wins += 1
#             elif 'L' in stat:
#                 losses += 1
#             else:
#                 ties += 1
#         # Completions
#         if count == cmp_index or (cmp_index == -1 and len(reg_game_statline) == 7):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if cmp_index != -1 else 0)
#         # Pass Attempts
#         if count == pass_att_index or (pass_att_index == -1 and len(reg_game_statline) == 8):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_att_index != -1 else 0)
#         # Completion percentage
#         if count == cmp_pct_index or (cmp_pct_index == -1 and len(reg_game_statline) == 9):
#             reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if cmp_pct_index != -1 else 0.0)
#         # Pass yards
#         if count == pass_yds_index or (pass_yds_index == -1 and len(reg_game_statline) == 10):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_yds_index != -1 else 0)
#         # Pass touchdowns
#         if count == pass_td_index or (pass_td_index == -1 and len(reg_game_statline) == 11):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_td_index != -1 else 0)
#         # Interceptions
#         if count == int_index or (int_index == -1 and len(reg_game_statline) == 12):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if int_index != -1 else 0)
#         # Passer rating
#         if count == rating_index or (rating_index == -1 and len(reg_game_statline) == 13):
#             reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rating_index != -1 else 0.0)
#         # Times sacked
#         if count == times_sacked_index or (times_sacked_index == -1 and len(reg_game_statline) == 14):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if times_sacked_index != -1 else 0)
#         # Sack yards
#         if count == sack_yds_index or (sack_yds_index == -1 and len(reg_game_statline) == 15):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if sack_yds_index != -1 else 0)
#         # Pass yards/att
#         if count == pass_yds_per_att_index or (pass_yds_per_att_index == -1 and len(reg_game_statline) == 16):
#             reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if pass_yds_per_att_index != -1 else 0.0)
#         # Rush attempts
#         if count == rush_att_index or (rush_att_index == -1 and len(reg_game_statline) == 17):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_att_index != -1 else 0)
#         # Rush yards
#         if count == rush_yds_index or (rush_yds_index == -1 and len(reg_game_statline) == 18):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_yds_index != -1 else 0)
#         # Rush yards/att
#         if count == rush_yds_per_att_index or (rush_yds_per_att_index == -1 and len(reg_game_statline) == 19):
#             reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rush_yds_per_att_index != -1 else 0.0)
#         # Rush touchdowns
#         if count == rush_td_index or (rush_td_index == -1 and len(reg_game_statline) == 20):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_td_index != -1 else 0)
#         # Fumbles
#         if count == fmb_index or (fmb_index == -1 and len(reg_game_statline) == 21):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_index != -1 else 0)
#         # Fumbles lost
#         if count == fmb_lost_index or (fmb_lost_index == -1 and len(reg_game_statline) == 22):
#             reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_lost_index != -1 else 0)
#         # If statline is complete, add it to regular season games and restart
#         if count == statline_length:
#             if len(reg_game_statline) > 0:
#                 regular_season_statlines.append(reg_game_statline)
#                 cursor.execute(f"""
#                     INSERT INTO PasserStatline (player_id, season, date, subseason, week, home_away, opponent,
#                     result, completions, pass_attempts, completion_percentage, pass_yards, pass_touchdowns,
#                     interceptions, passer_rating, times_sacked, sack_yards, pass_yards_per_att, rush_attempts,
#                     rush_yards, rush_yards_per_attempt, rush_touchdowns, fumbles, fumbles_lost)
#                     VALUES ({new_player_id}, {reg_game_statline[0]}, '{reg_game_statline[1]}', '{reg_game_statline[2]}',
#                             {reg_game_statline[3]}, '{reg_game_statline[4]}', '{reg_game_statline[5]}',
#                             '{reg_game_statline[6]}', {reg_game_statline[7]}, {reg_game_statline[8]},
#                             {reg_game_statline[9]}, {reg_game_statline[10]}, {reg_game_statline[11]},
#                             {reg_game_statline[12]}, {reg_game_statline[13]}, {reg_game_statline[14]},
#                             {reg_game_statline[15]}, {reg_game_statline[16]}, {reg_game_statline[17]},
#                             {reg_game_statline[18]}, {reg_game_statline[19]}, {reg_game_statline[20]},
#                             {reg_game_statline[21]}, {reg_game_statline[22]});
#                             """)
#                 reg_game_statline = []
#             count = 0
#
#     if player_has_playoff_career:
#         passer_postseason_table = all_tables[-1].find("tbody")
#         postseason_games = passer_postseason_table.find_all("td")
#
#         postseason_header = all_tables[-1].find("thead")
#         postseason_headers = postseason_header.find_all("th")
#
#         season_index = -1
#         date_index = -1
#         week_index = -1
#         home_away_index = -1
#         opponent_index = -1
#         result_index = -1
#         cmp_index = -1
#         pass_att_index = -1
#         cmp_pct_index = -1
#         pass_yds_index = -1
#         pass_td_index = -1
#         int_index = -1
#         rating_index = -1
#         times_sacked_index = -1
#         sack_yds_index = -1
#         pass_yds_per_att_index = -1
#         rush_att_index = -1
#         rush_yds_index = -1
#         rush_yds_per_att_index = -1
#         rush_td_index = -1
#         fmb_index = -1
#         fmb_lost_index = -1
#
#         statline_started = False
#         stat_index = 0
#         for header in postseason_headers:
#             if "year_id" == header.get("data-stat"):
#                 statline_started = True
#             if statline_started:
#                 stat_index += 1
#                 if "year_id" == header.get("data-stat"):
#                     season_index = stat_index
#                 if "game_date" == header.get("data-stat"):
#                     date_index = stat_index
#                 if "week_num" == header.get("data-stat"):
#                     week_index = stat_index
#                 if "game_location" == header.get("data-stat"):
#                     home_away_index = stat_index
#                 if "opp" == header.get("data-stat"):
#                     opponent_index = stat_index
#                 if "game_result" == header.get("data-stat"):
#                     result_index = stat_index
#                 if "pass_cmp" == header.get("data-stat"):
#                     cmp_index = stat_index
#                 if "pass_att" == header.get("data-stat"):
#                     pass_att_index = stat_index
#                 if "pass_cmp_perc" == header.get("data-stat"):
#                     cmp_pct_index = stat_index
#                 if "pass_yds" == header.get("data-stat"):
#                     pass_yds_index = stat_index
#                 if "pass_td" == header.get("data-stat"):
#                     pass_td_index = stat_index
#                 if "pass_int" == header.get("data-stat"):
#                     int_index = stat_index
#                 if "pass_rating" == header.get("data-stat"):
#                     rating_index = stat_index
#                 if "pass_sacked" == header.get("data-stat"):
#                     times_sacked_index = stat_index
#                 if "pass_sacked_yds" == header.get("data-stat"):
#                     sack_yds_index = stat_index
#                 if "pass_yds_per_att" == header.get("data-stat"):
#                     pass_yds_per_att_index = stat_index
#                 if "rush_att" == header.get("data-stat"):
#                     rush_att_index = stat_index
#                 if "rush_yds" == header.get("data-stat"):
#                     rush_yds_index = stat_index
#                 if "rush_yds_per_att" == header.get("data-stat"):
#                     rush_yds_per_att_index = stat_index
#                 if "rush_td" == header.get("data-stat"):
#                     rush_td_index = stat_index
#                 if "fumbles" == header.get("data-stat"):
#                     fmb_index = stat_index
#                 if "fumbles_lost" == header.get("data-stat"):
#                     fmb_lost_index = stat_index
#
#         statline_length = stat_index
#
#         all_indexes = [season_index, date_index, week_index, home_away_index, opponent_index, result_index, cmp_index,
#                        pass_att_index, cmp_pct_index, pass_yds_index, pass_td_index, int_index, rating_index,
#                        times_sacked_index, sack_yds_index, pass_yds_per_att_index, rush_att_index, rush_yds_index,
#                        rush_yds_per_att_index, rush_td_index, fmb_index, fmb_lost_index]
#
#         # all_indexes_names = ["season_index", "date_index", "week_index", "home_away_index", "opponent_index", "result_index", "cmp_index",
#         #                "pass_att_index", "cmp_pct_index", "pass_yds_index", "pass_td_index", "int_index", "rating_index",
#         #                "times_sacked_index", "sack_yds_index", "pass_yds_per_att_index", "rush_att_index", "rush_yds_index",
#         #                "rush_yds_per_att_index", "rush_td_index", "fmb_index", "fmb_lost_index"]
#         #
#         # for i in range(len(all_indexes_names)):
#         #     print(all_indexes_names[i] + ':', all_indexes[i])
#
#         postseason_statlines = []
#
#         # List of strings representing a playoff game's statline, containing (in this order):
#         # season, date, subseason, week, home/away, opponent, result, completions, pass attempts, completion %, pass yards,
#         # pass touchdowns, interceptions, passer rating, times sacked, sack yards, pass yards/att, rush attempts,
#         # rush yards, rush yards/attempt, rush touchdowns, fumbles, fumbles lost
#         post_game_statline = []
#         count = 0
#
#         for game_stat in postseason_games:
#             stat = game_stat.text
#             count += 1
#             # print(f"{count}: {stat}")
#
#             # If player was inactive or injured, skip to next statline
#             # if ("Inactive" in stat or "Injured" in stat or "Did" in stat
#             #         or "Suspended" in stat or "COVID" in stat or "Exempt" in stat):
#             if game_stat.get("data-stat") == "reason":
#                 count = statline_length
#                 post_game_statline = []
#             # Skip unused stats
#             if count not in all_indexes and count != statline_length:
#                 continue
#             # Season
#             if count == season_index or (season_index == -1 and len(post_game_statline) == 0):
#                 post_game_statline.append(int(stat) if season_index != -1 else 0)
#             # Date
#             if count == date_index or (date_index == -1 and len(post_game_statline) == 1):
#                 post_game_statline.append(stat if date_index != -1 else "0000-00-00")
#                 post_game_statline.append('post')
#             # Week
#             if count == week_index or (week_index == -1 and len(post_game_statline) == 3):
#                 post_game_statline.append(int(stat) if week_index != -1 else 0)
#             # Home/Away
#             if count == home_away_index or (home_away_index == -1 and len(post_game_statline) == 4):
#                 post_game_statline.append(("away" if '@' in stat else "home") if home_away_index != -1 else "unknown")
#             # Opponent
#             if count == opponent_index or (opponent_index == -1 and len(post_game_statline) == 5):
#                 post_game_statline.append(stat if opponent_index != -1 else "unknown")
#             # Result
#             if count == result_index or (result_index == -1 and len(post_game_statline) == 6):
#                 post_game_statline.append(stat if result_index != -1 else "unknown")
#             # Completions
#             if count == cmp_index or (cmp_index == -1 and len(post_game_statline) == 7):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if cmp_index != -1 else 0)
#             # Pass Attempts
#             if count == pass_att_index or (pass_att_index == -1 and len(post_game_statline) == 8):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_att_index != -1 else 0)
#             # Completion percentage
#             if count == cmp_pct_index or (cmp_pct_index == -1 and len(post_game_statline) == 9):
#                 post_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if cmp_pct_index != -1 else 0.0)
#             # Pass yards
#             if count == pass_yds_index or (pass_yds_index == -1 and len(post_game_statline) == 10):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_yds_index != -1 else 0)
#             # Pass touchdowns
#             if count == pass_td_index or (pass_td_index == -1 and len(post_game_statline) == 11):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_td_index != -1 else 0)
#             # Interceptions
#             if count == int_index or (int_index == -1 and len(post_game_statline) == 12):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if int_index != -1 else 0)
#             # Passer rating
#             if count == rating_index or (rating_index == -1 and len(post_game_statline) == 13):
#                 post_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rating_index != -1 else 0.0)
#             # Times sacked
#             if count == times_sacked_index or (times_sacked_index == -1 and len(post_game_statline) == 14):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if times_sacked_index != -1 else 0)
#             # Sack yards
#             if count == sack_yds_index or (sack_yds_index == -1 and len(post_game_statline) == 15):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if sack_yds_index != -1 else 0)
#             # Pass yards/att
#             if count == pass_yds_per_att_index or (pass_yds_per_att_index == -1 and len(post_game_statline) == 16):
#                 post_game_statline.append(
#                     (float(stat) if len(stat) > 0 else 0.0) if pass_yds_per_att_index != -1 else 0.0)
#             # Rush attempts
#             if count == rush_att_index or (rush_att_index == -1 and len(post_game_statline) == 17):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_att_index != -1 else 0)
#             # Rush yards
#             if count == rush_yds_index or (rush_yds_index == -1 and len(post_game_statline) == 18):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_yds_index != -1 else 0)
#             # Rush yards/att
#             if count == rush_yds_per_att_index or (rush_yds_per_att_index == -1 and len(post_game_statline) == 19):
#                 post_game_statline.append(
#                     (float(stat) if len(stat) > 0 else 0.0) if rush_yds_per_att_index != -1 else 0.0)
#             # Rush touchdowns
#             if count == rush_td_index or (rush_td_index == -1 and len(post_game_statline) == 20):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_td_index != -1 else 0)
#             # Fumbles
#             if count == fmb_index or (fmb_index == -1 and len(post_game_statline) == 21):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_index != -1 else 0)
#             # Fumbles lost
#             if count == fmb_lost_index or (fmb_lost_index == -1 and len(post_game_statline) == 22):
#                 post_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_lost_index != -1 else 0)
#             # If statline is complete, add it to regular season games and restart
#             if count == statline_length:
#                 if len(post_game_statline) > 0:
#                     postseason_statlines.append(post_game_statline)
#                     cursor.execute(f"""
#                         INSERT INTO PasserStatline (player_id, season, date, subseason, week, home_away, opponent,
#                         result, completions, pass_attempts, completion_percentage, pass_yards, pass_touchdowns,
#                         interceptions, passer_rating, times_sacked, sack_yards, pass_yards_per_att, rush_attempts,
#                         rush_yards, rush_yards_per_attempt, rush_touchdowns, fumbles, fumbles_lost)
#                         VALUES ({new_player_id}, {post_game_statline[0]}, '{post_game_statline[1]}', '{post_game_statline[2]}',
#                                 {post_game_statline[3]}, '{post_game_statline[4]}', '{post_game_statline[5]}',
#                                 '{post_game_statline[6]}', {post_game_statline[7]}, {post_game_statline[8]},
#                                 {post_game_statline[9]}, {post_game_statline[10]}, {post_game_statline[11]},
#                                 {post_game_statline[12]}, {post_game_statline[13]}, {post_game_statline[14]},
#                                 {post_game_statline[15]}, {post_game_statline[16]}, {post_game_statline[17]},
#                                 {post_game_statline[18]}, {post_game_statline[19]}, {post_game_statline[20]},
#                                 {post_game_statline[21]}, {post_game_statline[22]});
#                                 """)
#                     post_game_statline = []
#                 count = 0
#
#     # print("Regular season games:", len(regular_season_statlines))
#     # if player_has_playoff_career:
#     #     print("Playoff games:", len(postseason_statlines))
#
#     cursor.execute(f"""
#         UPDATE Player
#         SET wins = {wins}, losses = {losses}, ties = {ties}
#         WHERE player_id = {new_player_id};
#     """)
#
#     database.commit()

def insert_passer_statlines(passer_name, passer_draft_year, cursor, database):
    """Inserts data for all passers into Player and PasserStatline tables of the database

    :param passer_name: -> str
    :param passer_draft_year: -> str
    :param cursor: a cursor object connected to a valid SQL database
    :param database: the database to add tables to
    :return:
    """

    original_name = passer_name

    count = 0
    passer_link_number = "00"
    website_player_name = ""
    website_draft_year = ""
    receiving_included = False
    tackles_included = False
    punting_included = False
    player_has_playoff_career = False
    wins = 0
    losses = 0
    ties = 0
    include_periods = False
    passer_position = ""

    if "'" in passer_name:
        passer_name = passer_name.strip().replace("'", '')
    if "." in passer_name and int(passer_draft_year) <= 1974:
        passer_name = passer_name.strip().replace(".", '')

    # Since the pro football reference player url includes the number in order of when they were drafted compared to
    # players with similar names, we need to increment this number until their name and draft year match
    while website_player_name != passer_name or website_draft_year != passer_draft_year or "QB" not in passer_position:
        if original_name == "P.J. Walker":
            passer_gamelog_link = "https://www.pro-football-reference.com/players/W/WalkPh00/gamelog/"
        elif original_name == "Gino Torretta":
            passer_gamelog_link = "https://www.pro-football-reference.com/players/T/ToreGi00/gamelog/"
        elif len(passer_name.split(' ')) == 2:
            formatted_passer_name = passer_name.split(' ')[1][:4] + passer_name.split(' ')[0][:2] + str(
                int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[1]) > 3 \
                else (passer_name.split(' ')[-1] + 'x' * (4 - len(passer_name.split(' ')[1])) + passer_name.split(' ')[
                                                                                                    0][:2] +
                      str(int(passer_link_number) + count).zfill(2))
            passer_gamelog_link = f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[1][0]}/{formatted_passer_name}/gamelog/"
        # If player has three or more names (i.e. Billy Joe Redneck)
        else:
            # The website seems to randomly switch between using the second and third name, so if it doesn't work after
            # 20 tries with the third name, try the second
            if count < 21 and not include_periods:
                # Formatted name will be RednBi00
                formatted_passer_name = passer_name.split(' ')[-1][:4] + passer_name.split(' ')[0][:2] + str(
                    int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[-1]) > 3 else (
                        passer_name.split(' ')[-1][:4] + passer_name.split(' ')[0][:(4 - len(passer_name.split(' ')[1]))] +
                        passer_name.split(' ')[0][:2] +
                        str(int(passer_link_number) + count).zfill(2))
                passer_gamelog_link = (f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[-1][0]}/"
                                       f"{formatted_passer_name}/gamelog/")
            else:
                # Formatted name will be JoeRBi00
                if not include_periods:
                    count = 0
                include_periods = True
                formatted_passer_name = passer_name.split(' ')[1][:4] + passer_name.split(' ')[0][:2] + str(
                    int(passer_link_number) + count).zfill(2) if len(passer_name.split(' ')[1]) > 3 else (
                        passer_name.split(' ')[1] + passer_name.split(' ')[-1][:(4 - len(passer_name.split(' ')[1]))] +
                        passer_name.split(' ')[0][:2] + str(int(passer_link_number) + count).zfill(2))
                passer_gamelog_link = (f"https://www.pro-football-reference.com/players/{passer_name.split(' ')[1][0]}/"
                                       f"{formatted_passer_name}/gamelog/")
        print('.', end='')
        passer_gamelog_r = requests.get(passer_gamelog_link)
        passer_gamelog_page = BeautifulSoup(passer_gamelog_r.text, "lxml")
        # possible_error = passer_gamelog_page.find("div")
        # for i in possible_error:
        #     print(i.text)
        if count >= 10:
            time.sleep(10)
        passer_info = passer_gamelog_page.find("div", class_="players")
        if passer_info is None:
            if 4 < count < 19 and "." not in passer_name:
                count = 19
            else:
                count += 1
            continue
        website_player_name_tag = passer_info.find("h1")
        website_player_name = website_player_name_tag.text.strip().replace('\n', '').strip().replace("'", '')
        website_draft_year_tag = passer_info.find(attrs={"data-tip": True})
        if website_draft_year_tag is None:
            count += 1
            print(".", end='')
            continue
        website_draft_year = website_draft_year_tag.get("data-tip")[-4:] if '-' not in website_draft_year_tag.get(
            "data-tip") else website_draft_year_tag.get("data-tip")[-9:-5]
        passer_position_tag = passer_gamelog_page.find("strong", string="Position")
        passer_position = passer_position_tag.next_sibling.text
        # print(website_player_name, passer_name, website_draft_year, passer_draft_year, passer_position)
        count += 1


    all_tables = passer_gamelog_page.find_all("div", class_="table_wrapper")
    passer_all_stats = passer_gamelog_page.find("div", class_="table_wrapper")
    passer_regular_season_table = passer_all_stats.find("tbody")
    regular_season_games = passer_regular_season_table.find_all("td")

    regular_season_header = passer_all_stats.find("thead")
    regular_season_headers = regular_season_header.find_all("th")

    # If there are 2 tables on the page, one of them is for playoffs, and we need to process it
    if len(all_tables) == 2:
        player_has_playoff_career = True

    # Initialize required indexes to -1, so if they are not found in the statline, don't include them
    season_index = -1
    date_index = -1
    week_index = -1
    home_away_index = -1
    opponent_index = -1
    result_index = -1
    cmp_index = -1
    pass_att_index = -1
    cmp_pct_index = -1
    pass_yds_index = -1
    pass_td_index = -1
    int_index = -1
    rating_index = -1
    times_sacked_index = -1
    sack_yds_index = -1
    pass_yds_per_att_index = -1
    rush_att_index = -1
    rush_yds_index = -1
    rush_yds_per_att_index = -1
    rush_td_index = -1
    fmb_index = -1
    fmb_lost_index = -1

    statline_started = False
    stat_index = 0
    for header in regular_season_headers:
        if "year_id" == header.get("data-stat"):
            statline_started = True
        if statline_started:
            stat_index += 1
            # print(header.get("data-stat"))
            if "year_id" == header.get("data-stat"):
                season_index = stat_index
            if "game_date" == header.get("data-stat"):
                date_index = stat_index
            if "week_num" == header.get("data-stat"):
                week_index = stat_index
            if "game_location" == header.get("data-stat"):
                home_away_index = stat_index
            if "opp" == header.get("data-stat"):
                opponent_index = stat_index
            if "game_result" == header.get("data-stat"):
                result_index = stat_index
            if "pass_cmp" == header.get("data-stat"):
                cmp_index = stat_index
            if "pass_att" == header.get("data-stat"):
                pass_att_index = stat_index
            if "pass_cmp_perc" == header.get("data-stat"):
                cmp_pct_index = stat_index
            if "pass_yds" == header.get("data-stat"):
                pass_yds_index = stat_index
            if "pass_td" == header.get("data-stat"):
                pass_td_index = stat_index
            if "pass_int" == header.get("data-stat"):
                int_index = stat_index
            if "pass_rating" == header.get("data-stat"):
                rating_index = stat_index
            if "pass_sacked" == header.get("data-stat"):
                times_sacked_index = stat_index
            if "pass_sacked_yds" == header.get("data-stat"):
                sack_yds_index = stat_index
            if "pass_yds_per_att" == header.get("data-stat"):
                pass_yds_per_att_index = stat_index
            if "rush_att" == header.get("data-stat"):
                rush_att_index = stat_index
            if "rush_yds" == header.get("data-stat"):
                rush_yds_index = stat_index
            if "rush_yds_per_att" == header.get("data-stat"):
                rush_yds_per_att_index = stat_index
            if "rush_td" == header.get("data-stat"):
                rush_td_index = stat_index
            if "fumbles" == header.get("data-stat"):
                fmb_index = stat_index
            if "fumbles_lost" == header.get("data-stat"):
                fmb_lost_index = stat_index

    statline_length = stat_index

    all_indexes = [season_index, date_index, week_index, home_away_index, opponent_index, result_index, cmp_index,
                   pass_att_index, cmp_pct_index, pass_yds_index, pass_td_index, int_index, rating_index,
                   times_sacked_index, sack_yds_index, pass_yds_per_att_index, rush_att_index, rush_yds_index,
                   rush_yds_per_att_index, rush_td_index, fmb_index, fmb_lost_index]

    # all_indexes_names = ["season_index", "date_index", "week_index", "home_away_index", "opponent_index", "result_index", "cmp_index",
    #                "pass_att_index", "cmp_pct_index", "pass_yds_index", "pass_td_index", "int_index", "rating_index",
    #                "times_sacked_index", "sack_yds_index", "pass_yds_per_att_index", "rush_att_index", "rush_yds_index",
    #                "rush_yds_per_att_index", "rush_td_index", "fmb_index", "fmb_lost_index"]
    #
    # for i in range(len(all_indexes_names)):
    #     print(all_indexes_names[i] + ':', all_indexes[i])

    # regular_season_statlines = []

    # List of strings representing a regular season game's statline, containing (in this order):
    # season, date, subseason, week, home/away, opponent, result, completions, pass attempts, completion %, pass yards,
    # pass touchdowns, interceptions, passer rating, times sacked, sack yards, pass yards/att, rush attempts,
    # rush yards, rush yards/attempt, rush touchdowns, fumbles, fumbles lost
    reg_game_statline = []
    count = 0

    # Insert new player into database
    cursor.execute(f"""
            INSERT INTO Player (name, position, wins, losses, ties)
            VALUES ("{original_name}", 'Passer', {wins}, {losses}, {ties});
            """)

    new_player_id = cursor.lastrowid
    # print("ID:", new_player_id)

    for game_stat in regular_season_games:
        stat = game_stat.text
        count += 1
        # print(f"{count}: {stat}")

        # If player was inactive or injured, skip to next statline
        # if ("Inactive" in stat or "Injured" in stat or "Did" in stat
        #         or "Suspended" in stat or "COVID" in stat or "Exempt" in stat):
        if game_stat.get("data-stat") == "reason":
            count = statline_length
            reg_game_statline = []
        # Skip unused stats
        if count not in all_indexes and count != statline_length:
            continue
        # Season
        if count == season_index or (season_index == -1 and len(reg_game_statline) == 0):
            reg_game_statline.append(int(stat) if season_index != -1 else 0)
        # Date
        if count == date_index or (date_index == -1 and len(reg_game_statline) == 1):
            reg_game_statline.append(stat if date_index != -1 else "0000-00-00")
            reg_game_statline.append('reg')
        # Week
        if count == week_index or (week_index == -1 and len(reg_game_statline) == 3):
            reg_game_statline.append(int(stat) if week_index != -1 else 0)
        # Home/Away
        if count == home_away_index or (home_away_index == -1 and len(reg_game_statline) == 4):
            reg_game_statline.append(("away" if '@' in stat else "home") if home_away_index != -1 else "unknown")
        # Opponent
        if count == opponent_index or (opponent_index == -1 and len(reg_game_statline) == 5):
            reg_game_statline.append(stat if opponent_index != -1 else "unknown")
        # Result
        if count == result_index or (result_index == -1 and len(reg_game_statline) == 6):
            reg_game_statline.append(stat if result_index != -1 else "unknown")
            if 'W' in stat:
                wins += 1
            elif 'L' in stat:
                losses += 1
            else:
                ties += 1
        # Completions
        if count == cmp_index or (cmp_index == -1 and len(reg_game_statline) == 7):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if cmp_index != -1 else 0)
        # Pass Attempts
        if count == pass_att_index or (pass_att_index == -1 and len(reg_game_statline) == 8):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_att_index != -1 else 0)
        # Completion percentage
        if count == cmp_pct_index or (cmp_pct_index == -1 and len(reg_game_statline) == 9):
            reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if cmp_pct_index != -1 else 0.0)
        # Pass yards
        if count == pass_yds_index or (pass_yds_index == -1 and len(reg_game_statline) == 10):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_yds_index != -1 else 0)
        # Pass touchdowns
        if count == pass_td_index or (pass_td_index == -1 and len(reg_game_statline) == 11):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_td_index != -1 else 0)
        # Interceptions
        if count == int_index or (int_index == -1 and len(reg_game_statline) == 12):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if int_index != -1 else 0)
        # Passer rating
        if count == rating_index or (rating_index == -1 and len(reg_game_statline) == 13):
            reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rating_index != -1 else 0.0)
        # Times sacked
        if count == times_sacked_index or (times_sacked_index == -1 and len(reg_game_statline) == 14):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if times_sacked_index != -1 else 0)
        # Sack yards
        if count == sack_yds_index or (sack_yds_index == -1 and len(reg_game_statline) == 15):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if sack_yds_index != -1 else 0)
        # Pass yards/att
        if count == pass_yds_per_att_index or (pass_yds_per_att_index == -1 and len(reg_game_statline) == 16):
            reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if pass_yds_per_att_index != -1 else 0.0)
        # Rush attempts
        if count == rush_att_index or (rush_att_index == -1 and len(reg_game_statline) == 17):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_att_index != -1 else 0)
        # Rush yards
        if count == rush_yds_index or (rush_yds_index == -1 and len(reg_game_statline) == 18):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_yds_index != -1 else 0)
        # Rush yards/att
        if count == rush_yds_per_att_index or (rush_yds_per_att_index == -1 and len(reg_game_statline) == 19):
            reg_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rush_yds_per_att_index != -1 else 0.0)
        # Rush touchdowns
        if count == rush_td_index or (rush_td_index == -1 and len(reg_game_statline) == 20):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_td_index != -1 else 0)
        # Fumbles
        if count == fmb_index or (fmb_index == -1 and len(reg_game_statline) == 21):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_index != -1 else 0)
        # Fumbles lost
        if count == fmb_lost_index or (fmb_lost_index == -1 and len(reg_game_statline) == 22):
            reg_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_lost_index != -1 else 0)
        # If statline is complete, add it to regular season games and restart
        if count == statline_length:
            if len(reg_game_statline) > 0:
                total_stats = 0
                for i in range(7, len(reg_game_statline)):
                    total_stats += reg_game_statline[i]
                if total_stats != 0.0:
                    # regular_season_statlines.append(reg_game_statline)
                    cursor.execute(f"""
                        INSERT INTO PasserStatline (player_id, season, date, subseason, week, home_away, opponent, 
                        result, completions, pass_attempts, completion_percentage, pass_yards, pass_touchdowns,
                        interceptions, passer_rating, times_sacked, sack_yards, pass_yards_per_att, rush_attempts,
                        rush_yards, rush_yards_per_attempt, rush_touchdowns, fumbles, fumbles_lost)
                        VALUES ({new_player_id}, {reg_game_statline[0]}, '{reg_game_statline[1]}', '{reg_game_statline[2]}',
                                {reg_game_statline[3]}, '{reg_game_statline[4]}', '{reg_game_statline[5]}', 
                                '{reg_game_statline[6]}', {reg_game_statline[7]}, {reg_game_statline[8]}, 
                                {reg_game_statline[9]}, {reg_game_statline[10]}, {reg_game_statline[11]}, 
                                {reg_game_statline[12]}, {reg_game_statline[13]}, {reg_game_statline[14]}, 
                                {reg_game_statline[15]}, {reg_game_statline[16]}, {reg_game_statline[17]}, 
                                {reg_game_statline[18]}, {reg_game_statline[19]}, {reg_game_statline[20]}, 
                                {reg_game_statline[21]}, {reg_game_statline[22]});
                                """)
                reg_game_statline = []
            count = 0

    if player_has_playoff_career:
        passer_postseason_table = all_tables[-1].find("tbody")
        postseason_games = passer_postseason_table.find_all("td")

        postseason_header = all_tables[-1].find("thead")
        postseason_headers = postseason_header.find_all("th")

        season_index = -1
        date_index = -1
        week_index = -1
        home_away_index = -1
        opponent_index = -1
        result_index = -1
        cmp_index = -1
        pass_att_index = -1
        cmp_pct_index = -1
        pass_yds_index = -1
        pass_td_index = -1
        int_index = -1
        rating_index = -1
        times_sacked_index = -1
        sack_yds_index = -1
        pass_yds_per_att_index = -1
        rush_att_index = -1
        rush_yds_index = -1
        rush_yds_per_att_index = -1
        rush_td_index = -1
        fmb_index = -1
        fmb_lost_index = -1

        statline_started = False
        stat_index = 0
        for header in postseason_headers:
            if "year_id" == header.get("data-stat"):
                statline_started = True
            if statline_started:
                stat_index += 1
                if "year_id" == header.get("data-stat"):
                    season_index = stat_index
                if "game_date" == header.get("data-stat"):
                    date_index = stat_index
                if "week_num" == header.get("data-stat"):
                    week_index = stat_index
                if "game_location" == header.get("data-stat"):
                    home_away_index = stat_index
                if "opp" == header.get("data-stat"):
                    opponent_index = stat_index
                if "game_result" == header.get("data-stat"):
                    result_index = stat_index
                if "pass_cmp" == header.get("data-stat"):
                    cmp_index = stat_index
                if "pass_att" == header.get("data-stat"):
                    pass_att_index = stat_index
                if "pass_cmp_perc" == header.get("data-stat"):
                    cmp_pct_index = stat_index
                if "pass_yds" == header.get("data-stat"):
                    pass_yds_index = stat_index
                if "pass_td" == header.get("data-stat"):
                    pass_td_index = stat_index
                if "pass_int" == header.get("data-stat"):
                    int_index = stat_index
                if "pass_rating" == header.get("data-stat"):
                    rating_index = stat_index
                if "pass_sacked" == header.get("data-stat"):
                    times_sacked_index = stat_index
                if "pass_sacked_yds" == header.get("data-stat"):
                    sack_yds_index = stat_index
                if "pass_yds_per_att" == header.get("data-stat"):
                    pass_yds_per_att_index = stat_index
                if "rush_att" == header.get("data-stat"):
                    rush_att_index = stat_index
                if "rush_yds" == header.get("data-stat"):
                    rush_yds_index = stat_index
                if "rush_yds_per_att" == header.get("data-stat"):
                    rush_yds_per_att_index = stat_index
                if "rush_td" == header.get("data-stat"):
                    rush_td_index = stat_index
                if "fumbles" == header.get("data-stat"):
                    fmb_index = stat_index
                if "fumbles_lost" == header.get("data-stat"):
                    fmb_lost_index = stat_index

        statline_length = stat_index

        all_indexes = [season_index, date_index, week_index, home_away_index, opponent_index, result_index, cmp_index,
                       pass_att_index, cmp_pct_index, pass_yds_index, pass_td_index, int_index, rating_index,
                       times_sacked_index, sack_yds_index, pass_yds_per_att_index, rush_att_index, rush_yds_index,
                       rush_yds_per_att_index, rush_td_index, fmb_index, fmb_lost_index]

        # all_indexes_names = ["season_index", "date_index", "week_index", "home_away_index", "opponent_index", "result_index", "cmp_index",
        #                "pass_att_index", "cmp_pct_index", "pass_yds_index", "pass_td_index", "int_index", "rating_index",
        #                "times_sacked_index", "sack_yds_index", "pass_yds_per_att_index", "rush_att_index", "rush_yds_index",
        #                "rush_yds_per_att_index", "rush_td_index", "fmb_index", "fmb_lost_index"]
        #
        # for i in range(len(all_indexes_names)):
        #     print(all_indexes_names[i] + ':', all_indexes[i])

        # List of strings representing a playoff game's statline, containing (in this order):
        # season, date, subseason, week, home/away, opponent, result, completions, pass attempts, completion %, pass yards,
        # pass touchdowns, interceptions, passer rating, times sacked, sack yards, pass yards/att, rush attempts,
        # rush yards, rush yards/attempt, rush touchdowns, fumbles, fumbles lost
        post_game_statline = []
        count = 0

        for game_stat in postseason_games:
            stat = game_stat.text
            count += 1
            # print(f"{count}: {stat}")

            # If player was inactive or injured, skip to next statline
            # if ("Inactive" in stat or "Injured" in stat or "Did" in stat
            #         or "Suspended" in stat or "COVID" in stat or "Exempt" in stat):
            if game_stat.get("data-stat") == "reason":
                count = statline_length
                post_game_statline = []
            # Skip unused stats
            if count not in all_indexes and count != statline_length:
                continue
            # Season
            if count == season_index or (season_index == -1 and len(post_game_statline) == 0):
                post_game_statline.append(int(stat) if season_index != -1 else 0)
            # Date
            if count == date_index or (date_index == -1 and len(post_game_statline) == 1):
                post_game_statline.append(stat if date_index != -1 else "0000-00-00")
                post_game_statline.append('post')
            # Week
            if count == week_index or (week_index == -1 and len(post_game_statline) == 3):
                post_game_statline.append(int(stat) if week_index != -1 else 0)
            # Home/Away
            if count == home_away_index or (home_away_index == -1 and len(post_game_statline) == 4):
                post_game_statline.append(("away" if '@' in stat else "home") if home_away_index != -1 else "unknown")
            # Opponent
            if count == opponent_index or (opponent_index == -1 and len(post_game_statline) == 5):
                post_game_statline.append(stat if opponent_index != -1 else "unknown")
            # Result
            if count == result_index or (result_index == -1 and len(post_game_statline) == 6):
                post_game_statline.append(stat if result_index != -1 else "unknown")
            # Completions
            if count == cmp_index or (cmp_index == -1 and len(post_game_statline) == 7):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if cmp_index != -1 else 0)
            # Pass Attempts
            if count == pass_att_index or (pass_att_index == -1 and len(post_game_statline) == 8):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_att_index != -1 else 0)
            # Completion percentage
            if count == cmp_pct_index or (cmp_pct_index == -1 and len(post_game_statline) == 9):
                post_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if cmp_pct_index != -1 else 0.0)
            # Pass yards
            if count == pass_yds_index or (pass_yds_index == -1 and len(post_game_statline) == 10):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_yds_index != -1 else 0)
            # Pass touchdowns
            if count == pass_td_index or (pass_td_index == -1 and len(post_game_statline) == 11):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if pass_td_index != -1 else 0)
            # Interceptions
            if count == int_index or (int_index == -1 and len(post_game_statline) == 12):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if int_index != -1 else 0)
            # Passer rating
            if count == rating_index or (rating_index == -1 and len(post_game_statline) == 13):
                post_game_statline.append((float(stat) if len(stat) > 0 else 0.0) if rating_index != -1 else 0.0)
            # Times sacked
            if count == times_sacked_index or (times_sacked_index == -1 and len(post_game_statline) == 14):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if times_sacked_index != -1 else 0)
            # Sack yards
            if count == sack_yds_index or (sack_yds_index == -1 and len(post_game_statline) == 15):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if sack_yds_index != -1 else 0)
            # Pass yards/att
            if count == pass_yds_per_att_index or (pass_yds_per_att_index == -1 and len(post_game_statline) == 16):
                post_game_statline.append(
                    (float(stat) if len(stat) > 0 else 0.0) if pass_yds_per_att_index != -1 else 0.0)
            # Rush attempts
            if count == rush_att_index or (rush_att_index == -1 and len(post_game_statline) == 17):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_att_index != -1 else 0)
            # Rush yards
            if count == rush_yds_index or (rush_yds_index == -1 and len(post_game_statline) == 18):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_yds_index != -1 else 0)
            # Rush yards/att
            if count == rush_yds_per_att_index or (rush_yds_per_att_index == -1 and len(post_game_statline) == 19):
                post_game_statline.append(
                    (float(stat) if len(stat) > 0 else 0.0) if rush_yds_per_att_index != -1 else 0.0)
            # Rush touchdowns
            if count == rush_td_index or (rush_td_index == -1 and len(post_game_statline) == 20):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if rush_td_index != -1 else 0)
            # Fumbles
            if count == fmb_index or (fmb_index == -1 and len(post_game_statline) == 21):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_index != -1 else 0)
            # Fumbles lost
            if count == fmb_lost_index or (fmb_lost_index == -1 and len(post_game_statline) == 22):
                post_game_statline.append((int(stat) if len(stat) > 0 else 0) if fmb_lost_index != -1 else 0)
            # If statline is complete, add it to regular season games and restart
            if count == statline_length:
                if len(post_game_statline) > 0:
                    total_stats = 0
                    for i in range(7, len(post_game_statline)):
                        total_stats += post_game_statline[i]
                    if total_stats != 0.0:
                        cursor.execute(f"""
                            INSERT INTO PasserStatline (player_id, season, date, subseason, week, home_away, opponent, 
                            result, completions, pass_attempts, completion_percentage, pass_yards, pass_touchdowns,
                            interceptions, passer_rating, times_sacked, sack_yards, pass_yards_per_att, rush_attempts,
                            rush_yards, rush_yards_per_attempt, rush_touchdowns, fumbles, fumbles_lost)
                            VALUES ({new_player_id}, {post_game_statline[0]}, '{post_game_statline[1]}', '{post_game_statline[2]}',
                                    {post_game_statline[3]}, '{post_game_statline[4]}', '{post_game_statline[5]}', 
                                    '{post_game_statline[6]}', {post_game_statline[7]}, {post_game_statline[8]}, 
                                    {post_game_statline[9]}, {post_game_statline[10]}, {post_game_statline[11]}, 
                                    {post_game_statline[12]}, {post_game_statline[13]}, {post_game_statline[14]}, 
                                    {post_game_statline[15]}, {post_game_statline[16]}, {post_game_statline[17]}, 
                                    {post_game_statline[18]}, {post_game_statline[19]}, {post_game_statline[20]}, 
                                    {post_game_statline[21]}, {post_game_statline[22]});
                                    """)
                    post_game_statline = []
                count = 0

    #Update player wins, losses, ties
    cursor.execute(f"""
        UPDATE Player
        SET wins = {wins}, losses = {losses}, ties = {ties}
        WHERE player_id = {new_player_id};
    """)

    database.commit()

def delete_player_from_database(player_name, cursor, database):
    cursor.execute(f"""
        SELECT player_id
        FROM Player
        WHERE name = "{player_name}"
    """)
    id = cursor.fetchone()
    cursor.execute(f"""
        DELETE FROM PasserStatline
        WHERE player_id = {id[0]}
    """)
    cursor.execute(f"""
        DELETE FROM Player
        WHERE player_id = {id[0]}
    """)

    database.commit()


# start = time.time()
# insert_passer_statlines("Joe Namath", "1965", mycursor, db)
# insert_passer_statlines("Justin Fields", "2021")
# insert_passer_statlines("Josh Allen", "2018", mycursor, db)
# insert_passer_statlines("Bernie Kosar", "1985", mycursor, db)
# insert_passer_statlines("Dak Prescott", "2016")
# insert_passer_statlines("Brad Johnson", "1994")
# get_statline_size("Nick Foles", "2012")


# mycursor.execute("""
#     DELETE FROM Player;
# """)

# git ls-files | xargs wc -l

qb_names_years_dict = all_qbs_and_draft_years()

mycursor.execute("""
    SELECT name FROM player;
    """)
names = mycursor.fetchall()


def begin_manual_database_entry(qb_names_years_dict, mycursor, db):
    qbs_added_to_db = 0
    for qb in qb_names_years_dict.keys():  # range(len(qb_names_years_dict)):
        mycursor.execute("""
        SELECT name FROM player;
        """)
        names = mycursor.fetchall()
        qb_check = (qb,)
        if (qb_check in names):
            qbs_added_to_db += 1
            print(f"{qb} already in database")
            continue
        user_input = input(f"Add {qb} to the database? (type n to exit):")
        if user_input == '':
            qbs_added_to_db += 1
            start = time.time()
            insert_passer_statlines(qb, qb_names_years_dict[qb], mycursor, db)
            end = time.time()
            print(
                f"Successfully added {qb} to database. Took {round(end - start, 2)} seconds. {round(qbs_added_to_db / len(qb_names_years_dict), 3)}% complete")
        elif user_input == 'n':
            break


def begin_automatic_database_entry(qb_names_years_dict, mycursor, db):
    qbs_added_to_db = 0
    for qb in qb_names_years_dict.keys():  # range(len(qb_names_years_dict)):
        mycursor.execute("""
            SELECT name FROM player;
            """)
        names = mycursor.fetchall()
        qb_check = (qb,)
        if qb_check in names:
            qbs_added_to_db += 1
            print(f"{qb} already in database")
            continue
        random_float = random.uniform(5, 7)
        random_float_two_decimal = round(random_float, 2)
        time.sleep(random_float_two_decimal)
        print(f"Adding {qb}({qb_names_years_dict[qb]}) to database...", end='')
        qbs_added_to_db += 1
        start = time.time()
        insert_passer_statlines(qb, qb_names_years_dict[qb], mycursor, db)
        end = time.time()
        print(
            f"Successfully added {qb} to database. Took {round(end - start, 2)} seconds. {round((qbs_added_to_db / len(qb_names_years_dict)) * 100, 2)}% complete")


begin_automatic_database_entry(qb_names_years_dict, mycursor, db)
# insert_passer_statlines("Koy Detmer", "1998", mycursor, db)

# mycursor.execute("""
#     SELECT s.*
#     FROM Player p
#     JOIN PasserStatline s ON p.player_id = s.player_id
#     WHERE s.completions = 0 AND s.pass_attempts = 0 AND s.completion_percentage = 0.0
#     AND s.pass_yards = 0 AND s.pass_touchdowns = 0 AND s.interceptions = 0
#     AND s.passer_rating = 0.0 AND s.times_sacked = 0 AND s.sack_yards = 0
#     AND s.pass_yards_per_att = 0.0 AND s.rush_attempts = 0 AND s.rush_yards = 0
#     AND s.rush_yards_per_attempt = 0.0 AND s.rush_touchdowns = 0 AND s.fumbles = 0 AND s.fumbles_lost = 0
# """)
# empty_statlines = mycursor.fetchall()
# mycursor.execute("""
#     DELETE
#     FROM PasserStatline s
#     WHERE s.completions = 0 AND s.pass_attempts = 0 AND s.completion_percentage = 0.0
#     AND s.pass_yards = 0 AND s.pass_touchdowns = 0 AND s.interceptions = 0
#     AND s.passer_rating = 0.0 AND s.times_sacked = 0 AND s.sack_yards = 0
#     AND s.pass_yards_per_att = 0.0 AND s.rush_attempts = 0 AND s.rush_yards = 0
#     AND s.rush_yards_per_attempt = 0.0 AND s.rush_touchdowns = 0 AND s.fumbles = 0 AND s.fumbles_lost = 0
# """)

# print(len(players_with_no_attempts))
# mycursor.execute("""
#     SELECT DISTINCT p.name, s.*
#     FROM Player p
#     JOIN PasserStatline s ON p.player_id = s.player_id
#     WHERE s.pass_attempts = 0
# """)
# empty_statlines = mycursor.fetchall()
# print(len(empty_statlines))
# for i in players_with_no_attempts:
#     delete_player_from_database(i[0], mycursor, db)
# for i in empty_statlines:
#     print(i)

# insert_passer_statlines("Mike Busch", "1987", mycursor, db)
# insert_passer_statlines("Ken Anderson", "1971", mycursor, db)




# mycursor.execute("""
#     SELECT COUNT(ps.passer_statline_id)
#     FROM PasserStatline ps
#     JOIN Player p ON ps.player_id = p.player_id
#     WHERE p.name = 'Andrew Luck'
#     AND ps.result LIKE '%L%'
#     AND ps.subseason = 'reg';
# """)
# luck = mycursor.fetchall()
# print(luck)

# mycursor.execute("""
#             SELECT p.name, s.season, s.pass_yards, s.opponent, s.result
#             FROM Player p, PasserStatline s
#             WHERE p.player_id = s.player_id
#             AND s.pass_yards >= 500
#             AND s.subseason = 'post'
#             """)

# mycursor.execute("""
#     SELECT name, COUNT(*) as name_count
#     FROM Player
#     GROUP BY name
#     HAVING name_count > 1
# """)
mycursor.execute("""
    SELECT s.opponent, s.result, s.subseason
    FROM PasserStatline s, Player p
    WHERE p.player_id = s.player_id
    AND p.name = 'C.J. Stroud'
""")
dakplayoffs = mycursor.fetchall()
print(dakplayoffs)

# mycursor.execute("""
#     SELECT name
#     FROM Player
# """)
#
# yards = mycursor.fetchall()
# print(yards)
#
# mycursor.execute("""
#     SELECT p.name, s.result, s.opponent
#     FROM PasserStatline s, Player p
#     WHERE p.player_id = s.player_id
#     AND s.pass_yards >= 500
# """)
#
# yards = mycursor.fetchall()
# print(yards)

mycursor.close()
db.close()
