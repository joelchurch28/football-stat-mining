import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="ds373737",
    database="FootballStats"
)

mycursor = db.cursor()

def create_tables(cursor, database):
    """Creates the necessary tables for the database

    :param cursor: a cursor object connected to a valid SQL database
    :param database: the database to add tables to
    :return:
    """
    cursor.execute("""
    CREATE TABLE Player (
        player_id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        position ENUM('Passer', 'Receiver', 'Rusher') NOT NULL,
        wins INT DEFAULT 0,
        losses INT DEFAULT 0,
        ties INT DEFAULT 0
    );
    """)

    cursor.execute("""
    CREATE TABLE PasserStatline (
        passer_statline_id INT AUTO_INCREMENT PRIMARY KEY,
        player_id INT,
        season INT NOT NULL,
        date DATE NOT NULL,
        subseason ENUM('pre', 'reg', 'post') NOT NULL,
        week INT NOT NULL,
        home_away ENUM('home', 'away') NOT NULL,
        opponent VARCHAR(21) NOT NULL,
        result VARCHAR(8) NOT NULL,
        completions INT,
        pass_attempts INT,
        completion_percentage FLOAT,
        pass_yards INT,
        pass_touchdowns INT,
        interceptions INT,
        passer_rating FLOAT,
        times_sacked INT,
        sack_yards INT,
        pass_yards_per_att FLOAT,
        rush_attempts INT,
        rush_yards INT,
        rush_yards_per_attempt FLOAT,
        rush_touchdowns INT,
        fumbles INT,
        fumbles_lost INT,
        FOREIGN KEY (player_id) REFERENCES Player(player_id)
    );            
    """)

    cursor.execute("""
    CREATE TABLE RusherStatline (
        passer_statline_id INT AUTO_INCREMENT PRIMARY KEY,
        player_id INT,
        season INT NOT NULL,
        date DATE NOT NULL,
        subseason ENUM('pre', 'reg', 'post') NOT NULL,
        week INT NOT NULL,
        home_away ENUM('home', 'away') NOT NULL,
        opponent VARCHAR(21) NOT NULL,
        result VARCHAR(8) NOT NULL,
        rush_attempts INT,
        rush_yards INT,
        rush_yards_per_attempt FLOAT,
        rush_touchdowns INT,
        receptions INT,
        rec_yards INT,
        rec_yards_per_attempt FLOAT,
        rec_touchdowns INT,
        fumbles INT,
        fumbles_lost INT,
        FOREIGN KEY (player_id) REFERENCES Player(player_id)
    );
    """)

    cursor.execute("""
    CREATE TABLE ReceiverStatline (
        passer_statline_id INT AUTO_INCREMENT PRIMARY KEY,
        player_id INT,
        season INT NOT NULL,
        date DATE NOT NULL,
        subseason ENUM('pre', 'reg', 'post') NOT NULL,
        week INT NOT NULL,
        home_away ENUM('home', 'away') NOT NULL,
        opponent VARCHAR(21) NOT NULL,
        result VARCHAR(8) NOT NULL,
        receptions INT,
        rec_yards INT,
        rec_yards_per_reception FLOAT,
        rec_touchdowns INT,
        rush_attempts INT,
        rush_yards INT,
        rush_yards_per_attempt FLOAT,
        rush_touchdowns INT,
        fumbles INT,
        fumbles_lost INT,
        FOREIGN KEY (player_id) REFERENCES Player(player_id)
    );
    """)

    database.commit()

def delete_tables(cursor, database):
    """Deletes all tables in the database

    :param cursor: a cursor object connected to a valid SQL database
    :param database: the database to add tables to
    :return:
    """
    mycursor.execute("DROP TABLE IF EXISTS RusherStatline")
    mycursor.execute("DROP TABLE IF EXISTS ReceiverStatline")
    mycursor.execute("DROP TABLE IF EXISTS PasserStatline")
    mycursor.execute("DROP TABLE IF EXISTS Player")

    database.commit()

# delete_tables(mycursor, db)
# create_tables(mycursor, db)

mycursor.close()
db.close()