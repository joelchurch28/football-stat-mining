import unittest
from unittest.mock import patch, Mock
import requests
from bs4 import BeautifulSoup
from SearchFunctions.passingfunctions import *

class TestPassing(unittest.TestCase):
    @patch('requests.get')
    @patch('bs4.BeautifulSoup')
    def test_get_year_passing_dict(self, mock_bs, mock_get):
        # Setup mock response for requests.get
        mock_response = Mock()
        mock_response.text = """
        <html>
            <div class="nfl-c-player-info__value">2005-2020</div>
            <h3 class="d3-o-section-sub-title">Regular Season</h3>
            <table class="d3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}">
                <tr><td>Week 1</td><td>Stats</td></tr>
            </table>
        </html>
        """
        mock_get.return_value = mock_response

        # Setup mock BeautifulSoup object
        mock_soup = Mock()
        mock_bs.return_value = mock_soup

        # Mock the find_all method on the BeautifulSoup object
        mock_soup.find_all.side_effect = lambda tag, class_: {
            ('div', 'nfl-c-player-info__value'): [Mock(text="2005-2020")],
            ('h3', 'd3-o-section-sub-title'): [Mock(text=" Regular Season ")],
            ('table', 'd3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}'): [
                Mock(find_all=lambda tag: [Mock(text="Week 1"), Mock(text="Stats")])
            ]
        }[(tag, class_)]

        # Call the function
        result = get_year_passing_dict("Tom Brady", "2020")

        # Define the expected result
        expected_result = [{'RegularSeason': [['Week 1', 'Stats']]}]

        # Assert the result
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()