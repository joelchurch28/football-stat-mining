import re
import time
from bs4 import BeautifulSoup
import lxml
import requests
import pandas as pd

import SearchFunctions.passingfunctions
from Players.quarterback_class import Quarterback
from main import display_qb_comparison
from Filtered_Stats.filtered_pass_stats import PassStatsPerGame

start_time = time.time()

url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
r = requests.get(url)
soup = BeautifulSoup(r.text, "lxml")

table = soup.find("table", class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
headers = table.find_all("th")

categories = []
for header in headers:
    text = header.text
    textlist = text.split("\n")
    if (len(textlist) > 1 and len(textlist[1]) != 0):
        text = textlist[1]
    elif (len(textlist) == 1 and len(textlist[0]) != 0):
        text = textlist[0]
    else:
        print("Error adding header to list of headers")
    categories.append(text)

rowdata = []
rows = table.find_all("tr")

# FIND QB TEAM AND NUMBER
# List of all passing player page links on current page (will usually be 25 unless processing last page of table)
qb_links = table.find_all("a", class_="d3-o-player-fullname nfl-o-cta--link")
# print(len(qb_links))
tua_link = "https://www.nfl.com" + qb_links[0]['href']
qb_data_r = requests.get(tua_link)
qb_data_page = BeautifulSoup(qb_data_r.text, "lxml")
# qb_data_page = qb_data_soup.find_all("div", class_="d3-l-col__col-12")
tua_data_team = qb_data_page.find("div", class_="nfl-c-player-header__team nfl-u-hide-empty")
tua_data_num_pos = qb_data_page.find("div", class_="nfl-c-player-header__player-data nfl-u-hide-empty")
# print(tua_data_num_pos.text[tua_data_num_pos.text.index('#')+1:])#.strip().replace('\n', ''))
# print(tua_data_num_pos.text.strip().replace('\n', '')[:2])
# print(tua_data_team.text)

for i in rows[1:]:
    data = i.find_all("td")
    row = [tr.text.strip().replace('\n', '') for tr in data]
    rowdata.append(row)

tableYears = soup.find("select", class_="d3-o-dropdown")
currentYear = tableYears.find('option', selected=True).text

passing_year_dict = {}

# Locate the dropdown list of years with passing stats and put data into dictionary
list_years = soup.find("select", class_="d3-o-dropdown")
for i in list_years:
    year = i.text.strip().replace('\n', '')
    year_url = str(i)[15:82] if len(str(i)) == 97 else str(i)[27:94]
    if len(year) > 0:
        passing_year_dict[year] = year_url

qblist = []
# passer_links = []
passer_links = table.find_all("a", class_="d3-o-player-fullname nfl-o-cta--link")


# Add qb name team number and position to a list
# for col_no in range(len(rowdata)):
#     # print(len(statline))
#     statline = rowdata[col_no]
#     new_qb_link = "https://www.nfl.com" + passer_links[col_no]['href']
#     new_qb_r = requests.get(new_qb_link)
#     new_qb_page = BeautifulSoup(new_qb_r.text, "lxml")
#     new_qb_team_tag = new_qb_page.find("div", class_="nfl-c-player-header__team nfl-u-hide-empty")
#     new_qb_num_pos_tag = new_qb_page.find("div", class_="nfl-c-player-header__player-data nfl-u-hide-empty")
#     new_qb_team = new_qb_team_tag.text
#     new_qb_number = new_qb_num_pos_tag.text.strip().replace('\n', '')[
#                     new_qb_num_pos_tag.text.strip().replace('\n', '').index('#')+1:] if (
#                     '#' in new_qb_num_pos_tag.text) else "None"
#     newQB = Quarterback(statline[0], currentYear, new_qb_team, new_qb_number, statline[1], statline[2],
#                         statline[3], statline[4], statline[5], statline[6], statline[7], statline[8], statline[9],
#                         statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
#     qblist.append(newQB)

# for i in qblist:
#     print(i.name, i.forty_plus)

# 0 0 0 0 0 0 0 0 0 0 0 No next page 0 0 0 0 0 0 0 0 0 0

# next_table = soup.find("a", class_="nfl-o-table-pagination__next")
# if next_table is None:
#     print("DONE!")

# 0 0 0 0 0 0 0 0 0 0 List of years 0 0 0 0 0 0 0 0 0 0

# year_dict = {}
# tableYears = soup.find("select", class_="d3-o-dropdown")
# for i in tableYears:
#     year = i.text.strip().replace('\n', '')
#     year_url = str(i)[15:82]
#     if len(year) > 0:
#         year_dict[year] = year_url

# for i in range(len(years)):
#     print(str(i) + ":", years[i])
# for key in year_dict:
#     print(key, year_dict[key])
# print(len(rowdata))
# print(len(qblist))



# for i in qblist::

#     print(i.name, i.team, i.number)

def has_numbers(inputString):
    """Checks if a string contains an integer.

    :param inputString: The string to check for an integer
    :return: True if there's an integer, false if not
    """
    return any(char.isdigit() for char in inputString)


def qb_names_list(year=currentYear):

    qb_names = []

    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + str(year) + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    # Get the table on the first page
    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)
        qb_names.append(row[0])
        print(row[0])


    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")
    print("round done.")
    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        current_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        current_soup = BeautifulSoup(current_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        current_table = current_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = current_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)
            qb_names.append(row[0])
            print(row[0])

        next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return qb_names

def get_career_years(passer_name):
    career_years = []

    # Format the passer name to fit a link, removing any hyphens, periods, or apostrophes
    formatted_passer_name = (passer_name.strip().replace(" ", "-").lower().replace("'", "")
                             .replace(".", "-").replace("--", "-"))
    passer_link = "https://www.nfl.com/players/" + formatted_passer_name
    passer_r = requests.get(passer_link)
    passer_page = BeautifulSoup(passer_r.text, "lxml")

    # Find the most recent year given passer played
    passer_logs_link = passer_link + "/stats/logs"
    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    # If passer_season is empty, the player did not play in the most recent season,
    # and we need to append the last year to the end of the link
    passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

    years_passed = 1
    while len(passer_season) == 0:
        passer_logs_link = passer_logs_link = passer_link + "/stats/logs" + "/" + str(
            int(currentYear) - years_passed) + "/"
        passer_logs_r = requests.get(passer_logs_link)
        passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")
        passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

        years_passed += 1


    # Navigate to the next season of player's career, if the link doesn't have an integer in it, we're currently on the
    # most recent season, if it does have an integer we're in a past season
    # If we're in a year past, navigate to the page for one year earlier
    if has_numbers(passer_logs_link):
        passer_logs_link = passer_logs_link[:-5] + str(int(passer_logs_link[-5:-1]) - 1) + '/'
    # If we're in the current year, navigate to last year
    else:
        passer_logs_link = passer_logs_link + '/' + str(int(currentYear) - 1) + '/'
        career_years.append(currentYear)

    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    years = soup.find("select", class_="d3-o-dropdown")
    year_on = tableYears.find('option', selected=True).text

    # Keep adding dictionaries to career list until career is over (e.g. when passer_season is empty)
    while len(passer_season) != 0:

        career_years.append(passer_logs_link[-5:-1])

        passer_logs_link = passer_logs_link[:-5] + str(int(passer_logs_link[-5:-1]) - 1) + '/'
        passer_logs_r = requests.get(passer_logs_link)
        passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

        # If passer_season is empty, the player did not play in the most recent season,
        # and we need to append the last year to the end of the link
        passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

    # List of dictionaries with one to three keys and the same number of values.
    # Keys are strings 'Preseason', 'RegularSeason', and 'PostSeason'. Values are a list of games, each
    # of which is a list containing stats in this order, and can be null:
    # Week number, date, opponent, result, completions, attempts, yards, yds/att, td, int, sacks, sack yards,
    # passer rating, rush attempts, rush yards, rush yds/att, rush td, fumbles, fumbles lost
    return career_years

# dak23 = get_career_passing_dict("Dak Prescott")
# for i in dak23:
#     print(i.values())

# for i in dak23["RegularSeason"]:
# if(i[3][0] == 'W'):
#     print(i)

# get_passing_stats_vs_team("De'von Achane", "Bills")
# get_passing_stats_vs_team("C.J. Stroud", "Chiefs")
#dakvgiants = search_career_passing_dict("Dak v. Giants", get_career_passing_dict("Dak Prescott"), "Giants")
#qb_names_list()

#dak_career = get_career_passing_dict("Dak Prescott")
# dak_career = get_recent_passing_dict("Dak Prescott")
# dak_playoffs = search_career_passing_dict("Dak Prescott", dak_career, "wins")
# print(f"Rating: {dak_playoffs[0].rating}, record: {dak_playoffs[1]} - {dak_playoffs[2]} - {dak_playoffs[3]}")

# dakveaglesdict = search_career_passing_dict("Dak v. Eagles", get_career_passing_dict("Dak Prescott"), "Eagles")
# dakveagles = dakveaglesdict[0]
# dakveaglespergame = PassStatsPerGame(dakveagles.name, dakveagles.pass_yds, dakveagles.att, dakveagles.cmp, dakveagles.td, dakveagles.ints, dakveagles.yds_per_att, dakveagles.cmp_pct, dakveagles.rating, dakveaglesdict[1], dakveaglesdict[2], dakveaglesdict[3])
# for i in vars(dakveaglespergame).items():
#     print(i)
# dak20 = SearchFunctions.passingfunctions.get_year_passing_dict("Dak Prescott", "2020")
# print(SearchFunctions.passingfunctions.search_career_passing_dict("Dak Prescott", dak20))

def qb_names_list(year=currentYear):

    qb_names = []

    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + str(year) + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    # Get the table on the first page
    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)
        qb_names.append(row[0])

        formatted_passer_name = (row[0].strip().replace(" ", "-").lower().replace("'", "")
                                 .replace(".", "-").replace("--", "-")) if row[0] != "Josh Allen" else "josh-allen-4"
        passer_link = "https://www.nfl.com/players/" + formatted_passer_name
        passer_r = requests.get(passer_link)
        passer_page = BeautifulSoup(passer_r.text, "lxml")

        passer_position = passer_page.find_all("span", class_="nfl-c-player-header__position")
        print(passer_position)


    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")
    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        current_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        current_soup = BeautifulSoup(current_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        current_table = current_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = current_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)
            qb_names.append(row[0])

        next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return qb_names

def get_nonqb_year_passing_dict(player_name, year, player_link):

    non_qb = Quarterback(player_name)

    situational_link = player_link + '/stats/situational/' + year + '/'
    print(situational_link)
    player_r = requests.get(situational_link)
    player_page = BeautifulSoup(player_r.text, "html.parser")
    player_tables = player_page.find_all(class_="d3-l-col__col-12 nfl-t-stats--table nfl-t-stats--table-situational")

    for i in player_tables:
        if "Passing" in i.text:
            player_passing_table = (re.sub(r' {2,}', ' ', i.text.strip().replace('\n', '')))
            player_passing_table = player_passing_table[player_passing_table.index("Home Games"):player_passing_table.index("Margin")]
            break

    player_passing_home_games = player_passing_table[player_passing_table.index("Home Games") + 10 : player_passing_table.index("Road Games")]
    player_passing_road_games = player_passing_table[player_passing_table.index("Road Games") + 10 :]
    player_passing_home_statline = player_passing_home_games.split(' ')
    player_passing_road_statline = player_passing_road_games.split(' ')

    non_qb.att = int(player_passing_home_statline[1]) + int(player_passing_road_statline[1])
    non_qb.cmp = int(player_passing_home_statline[2]) + int(player_passing_road_statline[2])
    non_qb.pass_yds = int(player_passing_home_statline[4]) + int(player_passing_road_statline[4])
    non_qb.td = int(player_passing_home_statline[7]) + int(player_passing_road_statline[7])
    non_qb.ints = int(player_passing_home_statline[8]) + int(player_passing_road_statline[8])
    non_qb.firsts = int(player_passing_home_statline[9]) + int(player_passing_road_statline[9])


    # NEEDS RETURN, FORMATTING, and COMPLETED STATS above




print(time.time() - start_time)

