import datetime

from bs4 import BeautifulSoup
import requests
from Players import player_class, quarterback_class
import re
import time
from bs4 import BeautifulSoup
import lxml
import requests
import pandas as pd
from Players.quarterback_class import Quarterback
from PyQt5.QtWidgets import QApplication
import sys
import mysql.connector

url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"

# Open the NFL passing stats page and load it into a readable data structure
r = requests.get(url)
soup = BeautifulSoup(r.text, "lxml")

# Dictionary that stores years as keys and subsequent urls as the values
passing_year_dict = {}

# Locate the dropdown list of years with passing stats and put data into dictionary
list_years = soup.find("select", class_="d3-o-dropdown")
for i in list_years:
    year = i.text.strip().replace('\n', '')
    year_url = str(i)[15:82] if len(str(i)) == 97 else str(i)[27:94]
    if len(year) > 0:
        passing_year_dict[year] = year_url

tableYears = soup.find("select", class_="d3-o-dropdown")
currentYear = tableYears.find('option', selected=True).text


def first_page_passer_names(year=currentYear):
    """Returns a list of the first 25 quarterbacks with the most passing yards in the given year

    :param year: The year to check, most recent NFL season otherwise
    :return: list of 25 qbs
    """
    qb_names = []

    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + str(year) + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    # Get the table on the first page
    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)
        qb_names.append(row[0])

    return qb_names


def all_qbs_since_1970():
    """Returns a list of the names of any player who played the Quarterback Position from 1970 to today

    :return: list of names -> [str]
    """
    qb_names = []

    qb_names_link = "https://www.pro-football-reference.com/players/qbindex.htm"
    qb_names_r = requests.get(qb_names_link)
    qb_names_page = BeautifulSoup(qb_names_r.text, "lxml")
    qb_table_index = qb_names_page.find("div", class_="index")
    qb_table_rows = qb_table_index.find_all("tr")

    for row in qb_table_rows:
        # print(row.text)
        name = row.text.split("QB")[0]
        year = row.text[-8:-4]

        if '+' in name:
            name = name.strip().replace('+', '')
        if '-' in name:
            chars_erased = -1
            while (name[chars_erased] == '-' or name[chars_erased].isupper()):
                chars_erased -= 1
            name = name[:chars_erased + 1]
        if ' ' in name and int(year) >= 1970:
            qb_names.append(name)

    return qb_names


def all_passer_names_list(year=currentYear):
    """Returns a list of all the players with a pass attempt in the given year

    :param year: the year to search
    :return: a list of all the players with a pass attempt in the given year
    """

    qb_names = []

    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + str(year) + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    # Get the table on the first page
    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)
        qb_names.append(row[0])

    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")
    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        current_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        current_soup = BeautifulSoup(current_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        current_table = current_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = current_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)
            qb_names.append(row[0])

        next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return qb_names


def process_passing_current():
    """Cycles through each page of passing statistics for the CURRENT nfl season
    :return: qb_list is a list of Quarterback objects
    """
    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + currentYear + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    #
    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)

    table_years = current_soup.find("select", class_="d3-o-dropdown")
    current_year = table_years.find('option', selected=True).text

    qb_list = []

    # Find all the links to players' pages within the table
    passer_links = []
    passer_links = current_table.find_all("a", class_="d3-o-player-fullname nfl-o-cta--link")

    # Create a new Quarterback object with data from each row and add it to list that will be returned
    for col_no in range(len(row_data)):
        statline = row_data[col_no]
        new_qb_link = "https://www.nfl.com" + passer_links[col_no]['href']
        new_qb_r = requests.get(new_qb_link)
        new_qb_page = BeautifulSoup(new_qb_r.text, "lxml")
        new_qb_team_tag = new_qb_page.find("div", class_="nfl-c-player-header__team nfl-u-hide-empty")
        new_qb_num_pos_tag = new_qb_page.find("div", class_="nfl-c-player-header__player-data nfl-u-hide-empty")
        new_qb_team = new_qb_team_tag.text
        new_qb_number = new_qb_num_pos_tag.text.strip().replace('\n', '')[
                        new_qb_num_pos_tag.text.strip().replace('\n', '').index('#') + 1:] if (
                '#' in new_qb_num_pos_tag.text) else "None"
        new_qb = Quarterback(statline[0], current_year, new_qb_team, new_qb_number, statline[1], statline[2],
                             statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                             statline[9],
                             statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
        qb_list.append(new_qb)

    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")
    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        current_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        current_soup = BeautifulSoup(current_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        current_table = current_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = current_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)

        # Use table_years from outside function to find the current selected year
        current_year = table_years.find('option', selected=True).text

        # Find all the links to players' pages within the table
        passer_links = []
        passer_links = current_table.find_all("a", class_="d3-o-player-fullname nfl-o-cta--link")

        # Create a new Quarterback object with data from each row and add it to list that will be returned
        for col_no in range(len(row_data)):
            statline = row_data[col_no]
            new_qb_link = "https://www.nfl.com" + passer_links[col_no]['href']
            new_qb_r = requests.get(new_qb_link)
            new_qb_page = BeautifulSoup(new_qb_r.text, "lxml")
            new_qb_team_tag = new_qb_page.find("div", class_="nfl-c-player-header__team nfl-u-hide-empty")
            new_qb_num_pos_tag = new_qb_page.find("div", class_="nfl-c-player-header__player-data nfl-u-hide-empty")
            new_qb_team = new_qb_team_tag.text
            new_qb_number = new_qb_num_pos_tag.text.strip().replace('\n', '')[
                            new_qb_num_pos_tag.text.strip().replace('\n', '').index('#') + 1:] if (
                    '#' in new_qb_num_pos_tag.text) else "None"
            new_qb = Quarterback(statline[0], current_year, new_qb_team, new_qb_number, statline[1], statline[2],
                                 statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                 statline[9],
                                 statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
            qb_list.append(new_qb)
            print(col_no, "qb added.")

        next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return qb_list


def process_passing_current_without_teams():
    """Cycles through each page of passing statistics for the CURRENT nfl season
    :return: qb_list is a list of Quarterback objects
    """
    current_url = "https://www.nfl.com/stats/player-stats/category/passing/2023/reg/all/passingyards/desc"
    current_url = current_url[0:current_url.index("20")] + currentYear + '/' + current_url[current_url.index("reg"):]
    current_r = requests.get(current_url)
    current_soup = BeautifulSoup(current_r.text, "lxml")

    current_table = current_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

    row_data = []
    rows = current_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)

    table_years = current_soup.find("select", class_="d3-o-dropdown")
    current_year = table_years.find('option', selected=True).text

    qb_list = []

    # Create a new Quarterback object with data from each row and add it to list that will be returned
    for col_no in range(len(row_data)):
        statline = row_data[col_no]
        new_qb = Quarterback(statline[0], current_year, "No team given", "No number given", statline[1], statline[2],
                             statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                             statline[9],
                             statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
        qb_list.append(new_qb)

    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        current_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        current_soup = BeautifulSoup(current_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        current_table = current_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = current_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)

        # Use table_years from outside function to find the current selected year
        current_year = table_years.find('option', selected=True).text

        # Create a new Quarterback object with data from each row and add it to list that will be returned
        for col_no in range(len(row_data)):
            statline = row_data[col_no]
            new_qb = Quarterback(statline[0], current_year, "No team given", "No number given", statline[1],
                                 statline[2],
                                 statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                 statline[9],
                                 statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
            qb_list.append(new_qb)

        next_table = current_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return qb_list


def process_passing_year(given_year):
    """Cycles through each page of passing statistics for the given nfl season
    :param given_year: the year to process the passing stats for
    :return: qb_list is a list of Quarterback objects
    """
    key = str(given_year)
    if key not in passing_year_dict.keys():
        raise Exception("Passing stats for " + key + " not in NFL database")
    else:
        given_url = "https://www.nfl.com" + passing_year_dict[key]
        given_r = requests.get(given_url)
        given_soup = BeautifulSoup(given_r.text, "lxml")
        given_table = given_soup.find("table",
                                      class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                             "sortable")

        row_data = []
        if given_table is None:
            return []
        rows = given_table.find_all("tr")

        for j in rows[1:]:
            data = j.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)

        table_years = given_soup.find("select", class_="d3-o-dropdown")

        given_qb_list = []

        for statline in row_data:
            new_qb = Quarterback(statline[0], given_year, "No team given", "No number given", statline[1], statline[2],
                                 statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                 statline[9],
                                 statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
            given_qb_list.append(new_qb)

        # Find the next section on the table, if there is one.
        # If there isn't one, all passing stats have been processed.
        next_table = given_soup.find("a", class_="nfl-o-table-pagination__next")

        while next_table is not None:

            # Find the URL nested within the next button and navigate to it
            next_url = next_table['href']
            given_r = requests.get("https://www.nfl.com" + next_url)

            # Create a new nested structure from the new page's HTML document
            given_soup = BeautifulSoup(given_r.text, "lxml")

            # Find the statistics table on the page and extract all rows from it
            given_table = given_soup.find("table",
                                          class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
            rows = given_table.find_all("tr")

            # Clears all row data from previous pages
            row_data = []

            # Extract the data from the rows and reformat it into a list of strings,
            # then add that list to row_data
            for k in rows[1:]:
                data = k.find_all("td")
                row = [tr.text.strip().replace('\n', '') for tr in data]
                row_data.append(row)

            # Use table_years from outside function to find the current selected year
            given_year = table_years.find('option', selected=True).text

            # Create a new Quarterback object with data from each row and add it to list that will be returned
            for statline in row_data:
                new_qb = Quarterback(statline[0], given_year, "Dallas Cowboys", "4", int(statline[1]), statline[2],
                                     statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                     statline[9],
                                     statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])
                given_qb_list.append(new_qb)

            next_table = given_soup.find("a", class_="nfl-o-table-pagination__next")

        # If there is no next section, the table is concluded and return list of Quarterback objects
        return given_qb_list


def process_single_passer_year(passer_name, passer_year):
    # Check if the given name is correctly formatted; e.g. first and last name are capitalized and separated by a space
    test_name = passer_name.split(' ')
    if not test_name[0][0].isupper() or not test_name[1][0].isupper():
        raise Exception("Given name " + passer_name + " is incorrectly formatted")

    # Check if given year is in NFL's database, e.g. between 1970 and current year
    key = str(passer_year)
    if key not in passing_year_dict.keys():
        raise Exception("Passing stats for " + key + " not in NFL database")

    new_qb = Quarterback(passer_name)
    passer_url = "https://www.nfl.com" + passing_year_dict[key]
    passer_r = requests.get(passer_url)
    passer_soup = BeautifulSoup(passer_r.text, "lxml")
    passer_table = passer_soup.find("table",
                                    class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--"
                                           "sortable")

    row_data = []
    if passer_table is None:
        return []
    rows = passer_table.find_all("tr")

    for j in rows[1:]:
        data = j.find_all("td")
        row = [tr.text.strip().replace('\n', '') for tr in data]
        row_data.append(row)

    table_years = passer_soup.find("select", class_="d3-o-dropdown")

    # given_qb_list = []

    for statline in row_data:
        # print(statline)
        if statline[0] == passer_name:
            new_qb = Quarterback(statline[0], passer_year, "No team given", "No number given", statline[1], statline[2],
                                 statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                 statline[9],
                                 statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])

    # Find the next section on the table, if there is one.
    # If there isn't one, all passing stats have been processed.
    next_table = passer_soup.find("a", class_="nfl-o-table-pagination__next")

    while next_table is not None:

        # Find the URL nested within the next button and navigate to it
        next_url = next_table['href']
        passer_r = requests.get("https://www.nfl.com" + next_url)

        # Create a new nested structure from the new page's HTML document
        passer_soup = BeautifulSoup(passer_r.text, "lxml")

        # Find the statistics table on the page and extract all rows from it
        passer_table = passer_soup.find("table",
                                        class_="d3-o-table d3-o-table--detailed d3-o-player-stats--detailed d3-o-table--sortable")
        rows = passer_table.find_all("tr")

        # Clears all row data from previous pages
        row_data = []

        # Extract the data from the rows and reformat it into a list of strings,
        # then add that list to row_data
        for k in rows[1:]:
            data = k.find_all("td")
            row = [tr.text.strip().replace('\n', '') for tr in data]
            row_data.append(row)

        # Use table_years from outside function to find the current selected year
        passer_year = table_years.find('option', selected=True).text

        # Create a new Quarterback object with data from each row and add it to list that will be returned
        for statline in row_data:
            # print(statline)
            if statline[0] == passer_name:
                new_qb = Quarterback(statline[0], passer_year, "No team given", "No number given", statline[1],
                                     statline[2],
                                     statline[3], statline[4], statline[5], statline[6], statline[7], statline[8],
                                     statline[9],
                                     statline[10], statline[11], statline[12], statline[13], statline[14], statline[15])

        next_table = passer_soup.find("a", class_="nfl-o-table-pagination__next")

    # If there is no next section, the table is concluded and return list of Quarterback objects
    return new_qb


def process_passer_career(passer_name):
    """Compiles all the stats of a given player's career into one Quarterback object. Does not account for players who
        missed a year and came back for another reason. Needs better search algorithm for speed purposes

    :param passer_name: The name of the passer to compile the stats of
    :return: Quarterback instance with a given passer's career stats
    """
    # Test if the given name is correctly formatted; e.g. first and last name are capitalized and separated by a space
    test_name = passer_name.split(' ')
    if not test_name[0][0].isupper() or not test_name[1][0].isupper():
        raise Exception("Given name " + passer_name + " is incorrectly formatted")

    # Create a new Quarterback class to hold total stats, will add to each category year by year
    qb_career = Quarterback(passer_name)

    career_years = []
    years_played = 0
    career_status = "not started"
    for year in passing_year_dict.keys():
        in_this_year = False
        career_years = process_passing_year(year)
        if len(career_years) == 0:
            continue
        for passer in career_years:
            if passer.name == passer_name:
                in_this_year = True
                career_status = "started"
                years_played += 1
                qb_career.pass_yds += passer.pass_yds
                qb_career.att += passer.att
                qb_career.cmp += passer.cmp
                qb_career.td += passer.td
                qb_career.ints += passer.ints
                qb_career.rating += passer.rating
                qb_career.firsts += passer.firsts
                qb_career.twenty_plus += passer.twenty_plus
                qb_career.forty_plus += passer.forty_plus
                qb_career.lng = passer.lng if passer.lng > qb_career.lng else qb_career.lng
                qb_career.sck += passer.sck
                qb_career.sck_yds += qb_career.sck_yds
                continue
        if not in_this_year and career_status == "started":
            # Calculate career yards per attempt, completion percentage, and first down percentage
            qb_career.yds_per_att = qb_career.pass_yds / qb_career.att
            qb_career.cmp_pct = qb_career.cmp / qb_career.att
            qb_career.first_pct = qb_career.firsts / (qb_career.att + qb_career.sck)

            # Calculate career passer rating
            rating_a = ((qb_career.cmp / qb_career.att) - 0.3) * 5
            rating_b = ((qb_career.pass_yds / qb_career.att) - 3) * 0.25
            rating_c = (qb_career.td / qb_career.att) * 20
            rating_d = 2.375 - ((qb_career.ints / qb_career.att) * 25)
            qb_career.rating = ((rating_a + rating_b + rating_c + rating_d) / 6) * 100

            return qb_career
    return qb_career


def qb_compare(qb1_name, qb2_name, comparison_year):
    """Compares the stats between two quarterbacks. The better stat of the two becomes a string starting with the word
    "green", followed by the stat

    :param qb1_name: A string representing the name of the first passer being compared
    :param qb2_name: A string representing the name of the second passer being compared
    :param comparison_year: The year to compare the stats of the two passers, or "career" if you want to compare
    their careers
    :return: two Quarterback objects with modified stats
    """
    comparison_year = str(comparison_year)

    if comparison_year == "career":
        qb1 = process_passer_career(qb1_name)
        qb2 = process_passer_career(qb2_name)
    elif comparison_year in passing_year_dict.keys():
        qb1 = process_single_passer_year(qb1_name, comparison_year)
        qb2 = process_single_passer_year(qb2_name, comparison_year)
    else:
        raise Exception('Year must be between 1970 and the current year, or "career"')

    # Compare the stats between the two qbs, the better stat has "green" put in front of it for later identification

    # Pass yards
    qb1.pass_yds, qb2.pass_yds = compare_two_stats(qb1.pass_yds, qb2.pass_yds)
    # Yards per attempt
    qb1.yds_per_att, qb2.yds_per_att = compare_two_stats(qb1.yds_per_att, qb2.yds_per_att)
    # Pass attempts
    qb1.att, qb2.att = compare_two_stats(qb1.att, qb2.att)
    # Completions
    qb1.cmp, qb2.cmp = compare_two_stats(qb1.cmp, qb2.cmp)
    # Completion Percent
    qb1.cmp_pct, qb2.cmp_pct = compare_two_stats(qb1.cmp_pct, qb2.cmp_pct)
    # Touchdown passes
    qb1.td, qb2.td = compare_two_stats(qb1.td, qb2.td)
    # Interceptions, calculated differently as a higher interception total is worse
    if qb1.ints < qb2.ints:
        qb1.ints = "green" + str(qb1.ints)
    else:
        if qb2.ints != qb1.ints:
            qb2.ints = "green" + str(qb2.ints)
    # Passer rating
    qb1.rating, qb2.rating = compare_two_stats(qb1.rating, qb2.rating)
    # First downs
    qb1.firsts, qb2.firsts = compare_two_stats(qb1.firsts, qb2.firsts)
    # First down percentage
    qb1.first_pct, qb2.first_pct = compare_two_stats(qb1.first_pct, qb2.first_pct)
    # Completions of 20 yards or more
    qb1.twenty_plus, qb2.twenty_plus = compare_two_stats(qb1.twenty_plus, qb2.twenty_plus)
    # Completions of 40 yards or more
    qb1.forty_plus, qb2.forty_plus = compare_two_stats(qb1.forty_plus, qb2.forty_plus)
    # Longest completion
    qb1.lng, qb2.lng = compare_two_stats(qb1.lng, qb2.lng)
    # Sacks taken, less is better
    if qb1.sck < qb2.sck:
        qb1.sck = "green" + str(qb1.sck)
    else:
        if qb2.sck != qb1.sck:
            qb2.sck = "green" + str(qb2.sck)
    # Yards lost on sacks, less is better
    if qb1.sck_yds < qb2.sck_yds:
        qb1.sck_yds = "green" + str(qb1.sck_yds)
    else:
        if qb2.sck_yds != qb1.sck_yds:
            qb2.sck_yds = "green" + str(qb2.sck_yds)

    return qb1, qb2


def has_numbers(inputString):
    """Checks if a string contains an integer.

    :param inputString: The string to check for an integer
    :return: True if there's an integer, false if not
    """
    return any(char.isdigit() for char in inputString)


def compare_two_stats(stat1, stat2):
    """Compares two given statistics, the better one being assigned the word "green" in front of itself.
    Example: when comparing pass yards 1200 and 1000, they become "green1200" and "1000". If they are equal,
    they remain the same.

    :param stat1: an int or float representing some statistic
    :param stat2: an int or float representing another statistic
    :return: two strings, the one with the greater value being preceded by "green"
    """
    if stat1 > stat2:
        stat1 = "green" + str(stat1)
    else:
        if stat1 != stat2:
            stat2 = "green" + str(stat2)

    return stat1, stat2


def display_qb_comparison(first, second):
    """Prints the stats of two compared QBs side by side

    :param first: Quarterback instance of the first player being compared
    :param second: Quarterback instance of the second playet being compared
    :return: None
    """
    constant_length = len(first.name)

    print(" " * 25, first.name, "---", second.name)
    print((" " * 25), "-" * (5 + len(first.name) + len(second.name)))
    print("Pass yards:" + " " * (25 - len("Pass yards")) + str(first.pass_yds) + " " * (constant_length -
                                                                                        len(str(first.pass_yds))),
          "---", (" " * (len(second.name) - len(str(second.pass_yds)))) + str(second.pass_yds))
    print("Yards/attempt:" + " " * (25 - len("Yards/attempt")) + str(first.yds_per_att) + " " * (constant_length -
                                                                                                 len(str(
                                                                                                     first.yds_per_att))),
          "---", (" " * (len(second.name) - len(str(second.yds_per_att)))) +
          str(second.yds_per_att))
    print("Pass attempts:" + " " * (25 - len("Pass attempts")) + str(first.att) + " " * (constant_length -
                                                                                         len(str(first.att))), "---",
          (" " * (len(second.name) - len(str(second.att)))) + str(second.att))
    print("Completions:" + " " * (25 - len("Completions")) + str(first.cmp) + " " * (constant_length -
                                                                                     len(str(first.cmp))), "---",
          (" " * (len(second.name) - len(str(second.cmp)))) + str(second.cmp))
    print("Completion percentage:" + " " * (25 - len("Completion percentage")) + str(first.cmp_pct) + " " *
          (constant_length - len(str(first.cmp_pct))), "---", (" " * (len(second.name) - len(str(second.cmp_pct)))) +
          str(second.cmp_pct))
    print("Touchdowns:" + " " * (25 - len("Touchdowns")) + str(first.td) + " " * (constant_length -
                                                                                  len(str(first.td))), "---",
          (" " * (len(second.name) - len(str(second.td)))) + str(second.td))
    print("Interceptions:" + " " * (25 - len("Interceptions")) + str(first.ints) + " " * (constant_length -
                                                                                          len(str(first.ints))), "---",
          (" " * (len(second.name) - len(str(second.ints)))) + str(second.ints))
    print("Passer rating:" + " " * (25 - len("Passer rating")) + str(first.rating) + " " * (constant_length -
                                                                                            len(str(first.rating))),
          "---", (" " * (len(second.name) - len(str(second.rating)))) + str(second.rating))
    print("First downs:" + " " * (25 - len("First downs")) + str(first.firsts) + " " * (constant_length -
                                                                                        len(str(first.firsts))), "---",
          (" " * (len(second.name) - len(str(second.firsts)))) + str(second.firsts))
    print("First down percentage:" + " " * (25 - len("First down percentage")) + str(first.first_pct) + " " * (
                constant_length -
                len(str(first.first_pct))), "---",
          (" " * (len(second.name) - len(str(second.first_pct)))) + str(second.first_pct))


def get_recent_passing_dict(passer_name):
    """Given a player's name, creates a list containing a dictionary of their MOST RECENT season's passing
    statistics. The dictionary is in a list so it can be passed through the search career method.

    :param passer_name: A string containing a name of a quarterback/passer
    :return: A list containing a dictionary whose keys are "Preseason", "RegularSeason", and "PostSeason",
    and whose values are a list of statlines from each game played in their respective season.
    Each statline is also a list. If the player did not participate in one of these parts of the season,
    the dictionary will have one less key.
    """

    # Format the passer name to fit a link, removing any hyphens, periods, or apostrophes
    formatted_passer_name = (passer_name.strip().replace(" ", "-").lower().replace("'", "")
                             .replace(".", "-").replace("--", "-"))

    # As to not direct the page to that of Josh ALlen, defensive end
    if passer_name == "Josh Allen":
        formatted_passer_name = "josh-allen-4"

    passer_link = "https://www.nfl.com/players/" + formatted_passer_name
    passer_r = requests.get(passer_link)
    passer_page = BeautifulSoup(passer_r.text, "lxml")

    passer_info = passer_page.find_all("div", class_="nfl-c-player-info__value")
    passer_years_played = passer_info[4].text

    # Find the most recent year given passer played
    passer_logs_link = passer_link + "/stats/logs"
    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    # If passer_season is empty, the player did not play in the most recent season,
    # and we need to append the last year to the end of the link
    passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")
    years_passed = 1
    while len(passer_season) == 0:
        passer_logs_link = passer_logs_link = passer_link + "/stats/logs" + "/" + str(
            int(currentYear) - years_passed) + "/"
        passer_logs_r = requests.get(passer_logs_link)
        passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")
        passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

        years_passed += 1

    passer_season_tables = passer_logs_page.find_all("table",
                                                     class_="d3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}")
    passer_seasons_data = [tbl.find_all("td") for tbl in passer_season_tables]

    # Finds the titles of all the tables on the page, so we don't have to guess which stats are regular season, etc.
    season_titles_tags = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")
    season_titles = [szn.text.replace(' ', '').replace('\n', '') for szn in season_titles_tags]

    # list of lists, maximum three, representing stats from the preseason, regular season, and postseason.
    pre_reg_post = []

    for season in passer_seasons_data:
        season_stats = []
        statline = []
        for week in season:
            unformatted_stat = week.text.split('\n')
            statline.append(''.join(unformatted_stat).replace(' ', ''))
            if (len(statline) == 19):
                season_stats.append(statline)
                statline = []

        pre_reg_post.append(season_stats)

    # Create a dictionary with season_titles as the keys and pre_reg_post as the values
    # e.g. stats_by_season_type["RegularSeason"] == [[week 18 stats], [week 17 stats], ...]
    stats_by_season_type = {}
    for season_type in range(len(pre_reg_post)):
        stats_by_season_type[season_titles[season_type]] = pre_reg_post[season_type]

    # List containing a dictionary with one to three keys and the same number of values.
    # Keys are strings 'Preseason', 'RegularSeason', and 'PostSeason'. Values are a list of games, each
    # of which is a list containing stats in this order, and can be null:
    # Week number, date, opponent, result, completions, attempts, yards, yds/att, td, int, sacks, sack yards,
    # passer rating, rush attempts, rush yards, rush yds/att, rush td, fumbles, fumbles lost
    return [stats_by_season_type]

    # Create a dictionary with season_titles as the keys and pre_reg_post as the values
    # e.g. stats_by_season_type["RegularSeason"] == [[week 18 stats], [week 17 stats], ...]
    stats_by_season_type = {}
    for season_type in range(len(pre_reg_post)):
        stats_by_season_type[season_titles[season_type]] = pre_reg_post[season_type]

    # List containing a dictionary with one to three keys and the same number of values.
    # Keys are strings 'Preseason', 'RegularSeason', and 'PostSeason'. Values are a list of games, each
    # of which is a list containing stats in this order, and can be null:
    # Week number, date, opponent, result, completions, attempts, yards, yds/att, td, int, sacks, sack yards,
    # passer rating, rush attempts, rush yards, rush yds/att, rush td, fumbles, fumbles lost
    return [stats_by_season_type]


def get_year_passing_dict(passer_name, year):
    """Given a player's name, creates a list containing a dictionary of a given season's passing
    statistics. The dictionary is in a list so it can be passed through the search career method.

    :param passer_name: A string containing a name of a quarterback/passer
    :param year: the year to get stats from
    :return: A list containing a dictionary whose keys are "Preseason", "RegularSeason", and "PostSeason",
    and whose values are a list of statlines from each game played in their respective season.
    Each statline is also a list. If the player did not participate in one of these parts of the season,
    the dictionary will have one less key.
    """

    # Format the passer name to fit a link, removing any hyphens, periods, or apostrophes
    formatted_passer_name = (passer_name.strip().replace(" ", "-").lower().replace("'", "")
                             .replace(".", "-").replace("--", "-"))

    # As to not direct the page to that of Josh ALlen, defensive end
    if passer_name == "Josh Allen":
        formatted_passer_name = "josh-allen-4"

    passer_link = "https://www.nfl.com/players/" + formatted_passer_name
    passer_r = requests.get(passer_link)
    passer_page = BeautifulSoup(passer_r.text, "lxml")

    passer_info = passer_page.find_all("div", class_="nfl-c-player-info__value")
    # passer_years_played = passer_info[4].text

    # Find the most recent year given passer played
    passer_logs_link = passer_link + "/stats/logs" + '/' + year + '/'
    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

    # If passer season has a length of 0, the player did not play in the given year.
    if len(passer_season) == 0:
        return [{'Preseason': [['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']],
                 'RegularSeason': [['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']],
                 'PostSeason': [['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']]
                 }]

    passer_season_tables = passer_logs_page.find_all("table",
                                                     class_="d3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}")
    passer_seasons_data = [tbl.find_all("td") for tbl in passer_season_tables]

    # Finds the titles of all the tables on the page, so we don't have to guess which stats are regular season, etc.
    season_titles_tags = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")
    season_titles = [szn.text.replace(' ', '').replace('\n', '') for szn in season_titles_tags]

    # list of lists, maximum three, representing stats from the preseason, regular season, and postseason.
    pre_reg_post = []

    for season in passer_seasons_data:
        season_stats = []
        statline = []
        for week in season:
            unformatted_stat = week.text.split('\n')
            statline.append(''.join(unformatted_stat).replace(' ', ''))
            if (len(statline) == 19):
                season_stats.append(statline)
                statline = []

        pre_reg_post.append(season_stats)

    # Create a dictionary with season_titles as the keys and pre_reg_post as the values
    # e.g. stats_by_season_type["RegularSeason"] == [[week 18 stats], [week 17 stats], ...]
    stats_by_season_type = {}
    for season_type in range(len(pre_reg_post)):
        stats_by_season_type[season_titles[season_type]] = pre_reg_post[season_type]

    # List containing a dictionary with one to three keys and the same number of values.
    # Keys are strings 'Preseason', 'RegularSeason', and 'PostSeason'. Values are a list of games, each
    # of which is a list containing stats in this order, and can be null:
    # Week number, date, opponent, result, completions, attempts, yards, yds/att, td, int, sacks, sack yards,
    # passer rating, rush attempts, rush yards, rush yds/att, rush td, fumbles, fumbles lost
    return [stats_by_season_type]


def get_career_passing_dict(passer_name):
    """Given a player's name, creates a dictionary of their career's passing statistics

    :param passer_name: A string containing a name of a quarterback/passer
    :return: A list of dictionaries whose keys are "Preseason", "RegularSeason", and "PostSeason", and whose values are a list of
    statlines from each game played in their respective season. Each statline is also a list. If the player did not
    participate in one of these parts of the season, the dictionary will have one less key.
    """

    # Create the list to hold the dictionaries that will be returned
    career = []

    # Format the passer name to fit a link, removing any hyphens, periods, or apostrophes
    formatted_passer_name = (passer_name.strip().replace(" ", "-").lower().replace("'", "")
                             .replace(".", "-").replace("--", "-"))

    # As to not direct the page to that of Josh ALlen, defensive end
    if passer_name == "Josh Allen":
        formatted_passer_name = "josh-allen-4"

    passer_link = "https://www.nfl.com/players/" + formatted_passer_name
    passer_r = requests.get(passer_link)
    passer_page = BeautifulSoup(passer_r.text, "lxml")

    passer_info = passer_page.find_all("div", class_="nfl-c-player-info__value")
    # passer_years_played = passer_info[4].text
    print(passer_info)

    # Find the most recent year given passer played
    passer_logs_link = passer_link + "/stats/logs"
    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    # If passer_season is empty, the player did not play in the most recent season,
    # and we need to append the last year to the end of the link
    passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

    years_passed = 1
    while len(passer_season) == 0:
        passer_logs_link = passer_logs_link = passer_link + "/stats/logs" + "/" + str(
            int(currentYear) - years_passed) + "/"
        passer_logs_r = requests.get(passer_logs_link)
        passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")
        passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

        years_passed += 1

    passer_season_tables = passer_logs_page.find_all("table",
                                                     class_="d3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}")
    passer_seasons_data = [tbl.find_all("td") for tbl in passer_season_tables]

    # Finds the titles of all the tables on the page, so we don't have to guess which stats are regular season, etc.
    season_titles_tags = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")
    season_titles = [szn.text.replace(' ', '').replace('\n', '') for szn in season_titles_tags]

    # list of lists, maximum three, representing stats from the preseason, regular season, and postseason.
    pre_reg_post = []

    for season in passer_seasons_data:
        season_stats = []
        statline = []
        for week in season:
            unformatted_stat = week.text.split('\n')
            statline.append(''.join(unformatted_stat).replace(' ', ''))
            if (len(statline) == 19):
                season_stats.append(statline)
                statline = []

        pre_reg_post.append(season_stats)

    # Create a dictionary with season_titles as the keys and pre_reg_post as the values
    # e.g. stats_by_season_type["RegularSeason"] == [[week 18 stats], [week 17 stats], ...]
    stats_by_season_type = {}
    for season_type in range(len(pre_reg_post)):
        stats_by_season_type[season_titles[season_type]] = pre_reg_post[season_type]

    career.append(stats_by_season_type)

    # Navigate to the next season of player's career, if the link doesn't have an integer in it, we're currently on the
    # most recent season, if it does have an integer we're in a past season

    # If we're in a year past, navigate to the page for one year earlier
    if (has_numbers(passer_logs_link)) and passer_name != "Josh Allen":
        print(passer_logs_link)
        passer_logs_link = passer_logs_link[:-5] + str(int(passer_logs_link[-5:-1]) - 1) + '/'
    # If we're in the current year, navigate to last year
    else:
        passer_logs_link = passer_logs_link + '/' + str(int(currentYear) - 1) + '/'

    passer_logs_r = requests.get(passer_logs_link)
    passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

    # Keep adding dictionaries to career list until career is over (e.g. when passer_season is empty)
    while (len(passer_season) != 0):

        passer_season_tables = passer_logs_page.find_all("table",
                                                         class_="d3-o-table d3-o-standings--detailed d3-o-table--sortable {sortlist: [[0,1]]}")
        passer_seasons_data = [tbl.find_all("td") for tbl in passer_season_tables]

        # Finds the titles of all the tables on the page, so we don't have to guess which stats are regular season, etc.
        season_titles_tags = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")
        season_titles = [szn.text.replace(' ', '').replace('\n', '') for szn in season_titles_tags]

        # list of lists, maximum three, representing stats from the preseason, regular season, and postseason.
        pre_reg_post = []

        for season in passer_seasons_data:
            season_stats = []
            statline = []
            for week in season:
                unformatted_stat = week.text.split('\n')
                statline.append(''.join(unformatted_stat).replace(' ', ''))
                if (len(statline) == 19):
                    season_stats.append(statline)
                    statline = []

            pre_reg_post.append(season_stats)

        # Create a dictionary with season_titles as the keys and pre_reg_post as the values
        # e.g. stats_by_season_type["RegularSeason"] == [[week 18 stats], [week 17 stats], ...]
        stats_by_season_type = {}
        for season_type in range(len(pre_reg_post)):
            stats_by_season_type[season_titles[season_type]] = pre_reg_post[season_type]

        career.append(stats_by_season_type)

        passer_logs_link = passer_logs_link[:-5] + str(int(passer_logs_link[-5:-1]) - 1) + '/'
        passer_logs_r = requests.get(passer_logs_link)
        passer_logs_page = BeautifulSoup(passer_logs_r.text, "lxml")

        # If passer_season is empty, the player did not play in the most recent season,
        # and we need to append the last year to the end of the link
        passer_season = passer_logs_page.find_all("h3", class_="d3-o-section-sub-title")

    # List of dictionaries with one to three keys and the same number of values.
    # Keys are strings 'Preseason', 'RegularSeason', and 'PostSeason'. Values are a list of games, each
    # of which is a list containing stats in this order, and can be null:
    # Week number, date, opponent, result, completions, attempts, yards, yds/att, td, int, sacks, sack yards,
    # passer rating, rush attempts, rush yards, rush yds/att, rush td, fumbles, fumbles lost
    return career


def search_career_passing_dict(qb_name, career_list, filter="reg", include_playoffs=False):
    """ Searches a given career, in the form of a list of dictionaries representing seasons, and returns a list
    containing a Quarterback object containing only the specified stats, and their wins, losses, and ties in that window

    :param qb_name: the name of the given quarterback
    :param career_dict: A dictionary of a player's CAREER (not single season) to be searched
    :param filter: a string containing which stats to include, can be any of the following:
                'pre', 'reg', 'post', 'last10', 'home', 'away', or a team name. If none is given, it defaults to 'reg'
    :param include_playoffs: A boolean that determines whether playoff stats will be included
    :return: a list with a quarterback instance containing the stats from the dictionary that match the specified
    constraints, and three integers, one representing wins, one for losses, and ties
    """

    filters = ["pre", "reg", "post", "regpost", "last10", "home", "away", "wins", "losses"]
    teams = ["Cardinals",
             "Falcons",
             "Ravens",
             "Bills",
             "Panthers",
             "Bears",
             "Bengals",
             "Browns",
             "Cowboys",
             "Broncos",
             "Lions",
             "Packers",
             "Texans",
             "Colts",
             "Jaguars",
             "Chiefs",
             "Raiders",
             "Chargers",
             "Rams",
             "Dolphins",
             "Vikings",
             "Patriots",
             "Saints",
             "Giants",
             "Jets",
             "Eagles",
             "Steelers",
             "49ers",
             "Seahawks",
             "Buccaneers",
             "Titans",
             "Commanders"]
    if filter not in filters and filter not in teams:
        raise Exception("Filter '" + filter + "' not found")

    if career_list.__class__ != [].__class__:
        raise Exception("Given quarterback stats must be for their career")

    # Create the quarterback class to store particular stats
    filtered_quarterback = Quarterback(qb_name)
    # Initialize the quarterback's record in the specified range
    wins = 0
    losses = 0
    ties = 0

    for season in career_list:
        # If qb played in the preseason, and filter isn't regular season or postseason, iterate through games
        if "Preseason" in season and (filter != "reg" and filter != "post"):
            # Iterate through the preseason, game by game
            for preseason_game in season["Preseason"]:
                # Only analyze games where QB had a pass attempt, rush attempt, or sack
                if not (len(preseason_game[5]) == 0 and len(preseason_game[13]) == 0 and len(preseason_game[10]) == 0):
                    # Assign stats to variables for better readability
                    opponent = preseason_game[2]
                    result = preseason_game[3]
                    completions = preseason_game[4]
                    attempts = preseason_game[5]
                    yards = preseason_game[6]
                    # yards_per_att = preseason_game[7]
                    touchdowns = preseason_game[8]
                    interceptions = preseason_game[9]
                    sacks_taken = preseason_game[10]
                    sack_yards = preseason_game[11]
                    rush_attempts = preseason_game[13]
                    # passer_rating = preseason_game[12]

                    # Update record if the filter matches the team played or the filter is for all regular season games
                    # Update record according to filter:
                    #   if filter is a team -and- the game opponent is the team, add it
                    #   if filter is regular season, add it
                    #   if filter is away games -and- opponent contains '@', add it
                    #   if filter is home games -and- opponent does not contain '@', add it
                    #   if filter is wins -and- result contains 'W', add it
                    #   if filter is losses -and- result contains 'L', add it
                    if (filter == 'pre'):
                        if 'W' in preseason_game[3]:
                            wins += 1
                        elif 'L' in preseason_game[3]:
                            losses += 1
                        else:
                            ties += 1
                        if len(completions) != 0:
                            filtered_quarterback.cmp += int(completions)
                        if len(attempts) != 0:
                            filtered_quarterback.att += int(attempts)
                        if len(yards) != 0:
                            filtered_quarterback.pass_yds += int(yards)
                        if len(touchdowns) != 0:
                            filtered_quarterback.td += int(touchdowns)
                        if len(interceptions) != 0:
                            filtered_quarterback.ints += int(interceptions)
                        if len(sacks_taken) != 0:
                            filtered_quarterback.sck += int(sacks_taken)
                        if len(sack_yards) != 0:
                            filtered_quarterback.sck_yds += int(sack_yards)
                        if (filtered_quarterback.cmp > 0 and filtered_quarterback.att > 0
                                and filtered_quarterback.pass_yds > 0 and filtered_quarterback.td > 0):
                            # Calculate passer rating with above statistics
                            var_a = ((filtered_quarterback.cmp / filtered_quarterback.att) - 0.3) * 5
                            var_b = ((filtered_quarterback.pass_yds / filtered_quarterback.att) - 3) * 0.25
                            var_c = (filtered_quarterback.td / filtered_quarterback.att) * 20
                            var_d = 2.375 - ((filtered_quarterback.ints / filtered_quarterback.att) * 25)
                            filtered_quarterback.rating = round(((var_a + var_b + var_c + var_d) / 6) * 100, 1)
                            # Calculate completion percentage and yards per attempt
                            filtered_quarterback.cmp_pct = round(filtered_quarterback.cmp / filtered_quarterback.att, 2)
                            filtered_quarterback.yds_per_att = round(
                                filtered_quarterback.pass_yds / filtered_quarterback.att, 1)
        # If qb played in the regular season, and filter isn't preseason or postseason, iterate through games
        if "RegularSeason" in season and (filter != "pre" and filter != "post"):
            # Iterate through the regular season, game by game
            for regular_game in season["RegularSeason"]:
                # Only analyze games where QB had a pass attempt, rush attempt, or sack
                if not (len(regular_game[5]) == 0 and len(regular_game[13]) == 0 and len(regular_game[10]) == 0):
                    # Assign stats to variables for better readability
                    opponent = regular_game[2]
                    result = regular_game[3]
                    completions = regular_game[4]
                    attempts = regular_game[5]
                    yards = regular_game[6]
                    # yards_per_att = regular_game[7]
                    touchdowns = regular_game[8]
                    interceptions = regular_game[9]
                    sacks_taken = regular_game[10]
                    sack_yards = regular_game[11]
                    rush_attempts = regular_game[13]
                    # passer_rating = regular_game[12]

                    # Update record if the filter matches the team played or the filter is for all regular season games
                    # Update record according to filter:
                    #   if filter is a team -and- the game opponent is the team, add it
                    #   if filter is regular season, add it
                    #   if filter is away games -and- opponent contains '@', add it
                    #   if filter is home games -and- opponent does not contain '@', add it
                    #   if filter is wins -and- result contains 'W', add it
                    #   if filter is losses -and- result contains 'L', add it
                    if (((filter in opponent)
                         or (filter == "reg")
                         or (filter == "Commanders" and ("Redskins" in opponent or "Football" in opponent)))
                            or (filter == 'reg')
                            or (filter == "away" and '@' in opponent)
                            or (filter == "home" and '@' not in opponent)
                            or (filter == "wins" and 'W' in result)
                            or (filter == "losses" and 'L' in result)
                    ):
                        if 'W' in regular_game[3]:
                            wins += 1
                        elif 'L' in regular_game[3]:
                            losses += 1
                        else:
                            ties += 1
                        if len(completions) != 0:
                            filtered_quarterback.cmp += int(completions)
                        if len(attempts) != 0:
                            filtered_quarterback.att += int(attempts)
                        if len(yards) != 0:
                            filtered_quarterback.pass_yds += int(yards)
                        if len(touchdowns) != 0:
                            filtered_quarterback.td += int(touchdowns)
                        if len(interceptions) != 0:
                            filtered_quarterback.ints += int(interceptions)
                        if len(sacks_taken) != 0:
                            filtered_quarterback.sck += int(sacks_taken)
                        if len(sack_yards) != 0:
                            filtered_quarterback.sck_yds += int(sack_yards)
                        if (filtered_quarterback.cmp > 0 and filtered_quarterback.att > 0
                                and filtered_quarterback.pass_yds > 0 and filtered_quarterback.td > 0):
                            # Calculate passer rating with above statistics
                            var_a = ((filtered_quarterback.cmp / filtered_quarterback.att) - 0.3) * 5
                            var_b = ((filtered_quarterback.pass_yds / filtered_quarterback.att) - 3) * 0.25
                            var_c = (filtered_quarterback.td / filtered_quarterback.att) * 20
                            var_d = 2.375 - ((filtered_quarterback.ints / filtered_quarterback.att) * 25)
                            filtered_quarterback.rating = round(((var_a + var_b + var_c + var_d) / 6) * 100, 1)
                            # Calculate completion percentage and yards per attempt
                            filtered_quarterback.cmp_pct = round(filtered_quarterback.cmp / filtered_quarterback.att, 2)
                            filtered_quarterback.yds_per_att = round(
                                filtered_quarterback.pass_yds / filtered_quarterback.att, 1)

        # If qb played in the postseason, and filter isn't preseason or regular season, iterate through games
        if "PostSeason" in season and ((filter != "pre" or include_playoffs) and (filter != "reg" or include_playoffs)):
            # Iterate through the postseason, game by game
            for playoff_game in season["PostSeason"]:
                # Only analyze games where QB had a pass attempt, rush attempt, or sack
                if not (len(playoff_game[5]) == 0 and len(playoff_game[13]) == 0 and len(playoff_game[10]) == 0):
                    # Assign stats to variables for better readability
                    opponent = playoff_game[2]
                    result = playoff_game[3]
                    completions = playoff_game[4]
                    attempts = playoff_game[5]
                    yards = playoff_game[6]
                    # yards_per_att = playoff_game[7]
                    touchdowns = playoff_game[8]
                    interceptions = playoff_game[9]
                    sacks_taken = playoff_game[10]
                    sack_yards = playoff_game[11]
                    rush_attempts = playoff_game[13]
                    # passer_rating = playoff_game[12]

                    # Update record if the filter matches the team played or the filter is for all regular season games
                    # Update record according to filter:
                    #   if filter is a team -and- the game opponent is the team, add it
                    #   if filter is regular season, add it
                    #   if filter is away games -and- opponent contains '@', add it
                    #   if filter is home games -and- opponent does not contain '@', add it
                    #   if filter is wins -and- result contains 'W', add it
                    #   if filter is losses -and- result contains 'L', add it
                    if (
                            filter == "post"
                            or (filter == "reg" and include_playoffs)
                            or (filter == "pre" and include_playoffs)
                            or (filter in opponent and include_playoffs)
                            or (include_playoffs and (filter == "Commanders"
                                                      and ("Redskins" in opponent or "Football" in opponent)))
                            or (include_playoffs and filter == "wins" and 'W' in result)
                            or (include_playoffs and filter == "losses" and 'L' in result)
                            or (include_playoffs and filter == "away" and '@' in opponent)
                            or (include_playoffs and filter == "home" and '@' not in opponent)
                    ):
                        if 'W' in playoff_game[3]:
                            wins += 1
                        elif 'L' in playoff_game[3]:
                            losses += 1
                        else:
                            ties += 1
                        if len(completions) != 0:
                            filtered_quarterback.cmp += int(completions)
                        if len(attempts) != 0:
                            filtered_quarterback.att += int(attempts)
                        if len(yards) != 0:
                            filtered_quarterback.pass_yds += int(yards)
                        if len(touchdowns) != 0:
                            filtered_quarterback.td += int(touchdowns)
                        if len(interceptions) != 0:
                            filtered_quarterback.ints += int(interceptions)
                        if len(sacks_taken) != 0:
                            filtered_quarterback.sck += int(sacks_taken)
                        if len(sack_yards) != 0:
                            filtered_quarterback.sck_yds += int(sack_yards)
                        if (filtered_quarterback.cmp > 0 and filtered_quarterback.att > 0
                                and filtered_quarterback.pass_yds > 0 and filtered_quarterback.td > 0):
                            # Calculate passer rating with above statistics
                            var_a = ((filtered_quarterback.cmp / filtered_quarterback.att) - 0.3) * 5
                            var_b = ((filtered_quarterback.pass_yds / filtered_quarterback.att) - 3) * 0.25
                            var_c = (filtered_quarterback.td / filtered_quarterback.att) * 20
                            var_d = 2.375 - ((filtered_quarterback.ints / filtered_quarterback.att) * 25)
                            filtered_quarterback.rating = round(((var_a + var_b + var_c + var_d) / 6) * 100, 1)
                            # Calculate completion percentage and yards per attempt
                            filtered_quarterback.cmp_pct = round(filtered_quarterback.cmp / filtered_quarterback.att, 2)
                            filtered_quarterback.yds_per_att = round(
                                filtered_quarterback.pass_yds / filtered_quarterback.att, 1)

    return [filtered_quarterback, wins, losses, ties]


def search_career_database_passing(cursor, db, qb_name, filter="reg", year="career", include_playoffs=False):
    """ Searches a given career, in the form of a list of dictionaries representing seasons, and returns a list
    containing a Quarterback object containing only the specified stats, and their wins, losses, and ties in that window

    :param cursor: a cursor object connected to a valid SQL database
    :param db: the database to add tables to
    :param qb_name: the name of the given quarterback
    :param filter: a string containing which stats to include, can be any of the following:
                'pre', 'reg', 'post', 'last10', 'home', 'away', or a team name. If none is given, it defaults to 'reg'
    :param year: A string containing the year to analyze. Defaults to "career"
    :param include_playoffs: A boolean that determines whether playoff stats will be included
    :return: a list representing a statline with the following values: completions, pass attempts,
    completion percentage, pass yards, pass td, interceptions, passer rating, times sacked, sack yards, pass yards/att,
    rush attempts, rush yards, rush yards/att, rush td, fumbles, and fumbles lost. After this list there are three
    integers, one representing wins, one for losses, and ties
    """

    filters = ["pre", "reg", "post", "last10", "home", "away", "wins", "losses"]
    opponents = [
        "Cardinals", "Falcons", "Ravens", "Bills", "Panthers", "Bears", "Bengals", "Browns",
        "Cowboys", "Broncos", "Lions", "Packers", "Texans", "Colts", "Jaguars", "Chiefs",
        "Raiders", "Chargers", "Rams", "Dolphins", "Vikings", "Patriots", "Saints", "Giants",
        "Jets", "Eagles", "Steelers", "49ers", "Seahawks", "Buccaneers", "Titans", "Commanders"
    ]
    filter_keywords = {"None": "reg",
                       "Playoffs": "post",
                       "Home games": "home",
                       "Road games": "away",
                       "Wins": "wins",
                       "Losses": "losses",
                       "Cardinals": "ARI",
                       "Falcons": "ATL",
                       "Ravens": "BAL",
                       "Bills": "BUF",
                       "Panthers": "CAR",
                       "Bears": "CHI",
                       "Bengals": "CIN",
                       "Browns": "CLE",
                       "Cowboys": "DAL",
                       "Broncos": "DEN",
                       "Lions": "DET",
                       "Packers": "GNB",
                       "Texans": "HOU",
                       "Colts": "IND",
                       "Jaguars": "JAX",
                       "Chiefs": "KAN",
                       "Raiders": "LVR",
                       "Chargers": "LAC",
                       "Rams": "LAR",
                       "Dolphins": "MIA",
                       "Vikings": "MIN",
                       "Patriots": "NWE",
                       "Saints": "NOR",
                       "Giants": "NYG",
                       "Jets": "NYJ",
                       "Eagles": "PHI",
                       "Steelers": "PIT",
                       "49ers": "SFO",
                       "Seahawks": "SEA",
                       "Buccaneers": "TAM",
                       "Titans": "TEN",
                       "Commanders": "WAS"
                       }

    if filter not in filters and filter not in filter_keywords:
        raise Exception("Filter '" + filter + "' not found")

    # print(filter, year)

    # Handle all career searches
    if year == "career":
        # Handle all regular season games
        if filter_keywords[filter] == "reg" and not include_playoffs:
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked, 
                s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND s.subseason = 'reg'
            """)
        # Handle all regular season and playoff games
        elif filter_keywords[filter] == "reg" and include_playoffs:
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked, 
                s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND (s.subseason = 'reg' OR s.subseason = 'post')
            """)
        # Handle only playoff games
        elif filter_keywords[filter] == "post":
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked, 
                s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND s.subseason = 'post'
            """)
        elif filter_keywords[filter] == "home" or filter_keywords[filter] == "away":
            # All home games, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.home_away = '{filter_keywords[filter]}'
                """)
            # Only regular season home games
            else:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.subseason = 'reg'
                    AND s.home_away = '{filter_keywords[filter]}'
                """)
        elif filter_keywords[filter] == "wins" or filter_keywords[filter] == "losses":
            # All wins, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.result LIKE '%{filter_keywords[filter][0]}%'
                """)
            # Only regular season wins
            else:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.result LIKE '%{filter_keywords[filter][0]}%'
                    AND s.subseason = 'reg'
                """)
        elif filter in opponents:
            # All games vs. the given team, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                               SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                               s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                               s.fumbles_lost, s.result
                               FROM Player p
                               JOIN PasserStatline s ON p.player_id = s.player_id
                               WHERE p.name = "{qb_name}"
                               AND s.opponent LIKE '%{filter_keywords[filter]}%'
                           """)
            # Only regular season games vs. given team
            else:
                career_query = cursor.execute(f"""
                               SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                               s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                               s.fumbles_lost, s.result
                               FROM Player p
                               JOIN PasserStatline s ON p.player_id = s.player_id
                               WHERE p.name = "{qb_name}"
                               AND s.opponent LIKE '%{filter_keywords[filter]}%'
                               AND s.subseason = 'reg'
                           """)
    # Handle a single year's worth of statlines
    else:
        # Handle all regular season games
        if filter_keywords[filter] == "reg" and not include_playoffs:
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked, 
                s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND s.subseason = 'reg'
                AND s.season = {int(year)}
            """)
        # Handle all regular season and playoff games
        elif filter_keywords[filter] == "reg" and include_playoffs:
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked,
                 s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND s.season = {int(year)}
            """)
        # Handle only playoff games
        elif filter_keywords[filter] == "post":
            career_query = cursor.execute(f"""
                SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, s.times_sacked,
                 s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, s.fumbles_lost, s.result
                FROM Player p
                JOIN PasserStatline s ON p.player_id = s.player_id
                WHERE p.name = "{qb_name}"
                AND s.subseason = 'post'
                AND s.season = {int(year)}
            """)
        elif filter_keywords[filter] == "home" or filter_keywords[filter] == "away":
            # All home games, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.home_away = '{filter_keywords[filter]}'
                    AND s.season = {int(year)}
                """)
            # Only regular season home games
            else:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.subseason = 'reg'
                    AND s.home_away = '{filter_keywords[filter]}'
                    AND s.season = {int(year)}
                """)
        elif filter_keywords[filter] == "wins" or filter_keywords[filter] == "losses":
            # All wins, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.result LIKE '%{filter_keywords[filter][0]}%'
                    AND s.season = {int(year)}
                """)
            # Only regular season wins
            else:
                career_query = cursor.execute(f"""
                    SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                    s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, s.fumbles, 
                    s.fumbles_lost, s.result
                    FROM Player p
                    JOIN PasserStatline s ON p.player_id = s.player_id
                    WHERE p.name = "{qb_name}"
                    AND s.result LIKE '%{filter_keywords[filter][0]}%'
                    AND s.subseason = 'reg'
                    AND s.season = {int(year)}
                """)
        elif filter in opponents:
            # All games vs. the given team, including playoffs
            if include_playoffs:
                career_query = cursor.execute(f"""
                               SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                               s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, 
                               s.fumbles, s.fumbles_lost, s.result
                               FROM Player p
                               JOIN PasserStatline s ON p.player_id = s.player_id
                               WHERE p.name = "{qb_name}"
                               AND s.opponent LIKE '%{filter[filter]}%'
                               AND s.season = {int(year)}
                           """)
            # Only regular season games vs. given team
            else:
                career_query = cursor.execute(f"""
                               SELECT s.completions, s.pass_attempts, s.pass_yards, s.pass_touchdowns, s.interceptions, 
                               s.times_sacked, s.sack_yards, s.rush_attempts, s.rush_yards, s.rush_touchdowns, 
                               s.fumbles, s.fumbles_lost, s.result
                               FROM Player p
                               JOIN PasserStatline s ON p.player_id = s.player_id
                               WHERE p.name = "{qb_name}"
                               AND s.opponent LIKE '%{filter_keywords[filter]}%'
                               AND s.subseason = 'reg'
                               AND s.season = {int(year)}
                           """)
    career_list = cursor.fetchall()

    # Create the quarterback class to store particular stats
    #
    filtered_statline = [0, 0, 0.0, 0, 0, 0, 0.0, 0, 0, 0.0, 0, 0, 0.0, 0, 0, 0]
    # Initialize the quarterback's record in the specified range
    wins = 0
    losses = 0
    ties = 0

    # If no games were played by the qb under the specified constraints, return an empty Quarterback
    if len(career_list) == 0:
        return [filtered_statline, wins, losses, ties]

    for statline in career_list:
        # Assign stats to variables for better readability
        completions = statline[0]
        pass_attempts = statline[1]
        pass_yards = statline[2]
        pass_touchdowns = statline[3]
        interceptions = statline[4]
        times_sacked = statline[5]
        sack_yards = statline[6]
        rush_attempts = statline[7]
        rush_yards = statline[8]
        rush_touchdowns = statline[9]
        fumbles = statline[10]
        fumbles_lost = statline[11]
        result = statline[12]

        filtered_statline[0] += completions
        filtered_statline[1] += pass_attempts
        filtered_statline[3] += pass_yards
        filtered_statline[4] += pass_touchdowns
        filtered_statline[5] += interceptions
        filtered_statline[7] += times_sacked
        filtered_statline[8] += sack_yards
        filtered_statline[10] += rush_attempts
        filtered_statline[11] += rush_yards
        filtered_statline[13] += rush_touchdowns
        filtered_statline[14] += fumbles
        filtered_statline[15] += fumbles_lost

        if "W" in result:
            wins += 1
        elif "L" in result:
            losses += 1
        else:
            ties += 1

    if pass_attempts > 0:
        # completion percentage
        filtered_statline[2] = round(filtered_statline[0] / filtered_statline[1], 2)

        # passer rating
        var_a = ((filtered_statline[0] / filtered_statline[1]) - 0.3) * 5
        var_b = ((filtered_statline[3] / filtered_statline[1]) - 3) * 0.25
        var_c = (filtered_statline[4] / filtered_statline[1]) * 20
        var_d = 2.375 - ((filtered_statline[5] / filtered_statline[1]) * 25)
        filtered_statline[6] = round(((var_a + var_b + var_c + var_d) / 6) * 100, 1)

        # pass yards/att
        filtered_statline[9] = round(filtered_statline[3] / filtered_statline[1], 1)

        # rush yards/att
        filtered_statline[12] = round(filtered_statline[11] / filtered_statline[10], 1) if rush_attempts > 0 else 0.0
    else:
        # completion percentage
        filtered_statline[2], filtered_statline[6], filtered_statline[9], filtered_statline[12] = 0.0, 0.0, 0.0, 0.0

    return [filtered_statline, wins, losses, ties]


# db = mysql.connector.connect(
#     host="localhost",
#     user="root",
#     passwd="ds373737",
#     database="FootballStats"
# )
#
# mycursor = db.cursor()
# dak = search_career_database_passing(mycursor, db, "Dak Prescott", "Vikings", "career", True)
# print(dak)

def is_qb(player_name):
    """Checks if the given name belongs to that of an NFL quarterback

    :param name: the name of the player -> str
    :return: true if the player is a quarterback, false otherwise
    """
    if len(player_name) == 0:
        return False

    player_name = player_name.lower()

    # Format the passer name to fit a link, removing any hyphens, periods, or apostrophes
    formatted_passer_name = (player_name.strip().replace(" ", "-").replace("'", "")
                             .replace(".", "-").replace("--", "-")) if player_name != "josh allen" else "josh-allen-4"

    player_link = "https://www.nfl.com/players/" + formatted_passer_name
    player_r = requests.get(player_link)
    player_page = BeautifulSoup(player_r.text, "lxml")

    passer_info = player_page.find_all("div", class_="nfl-c-player-info__value")
    player_position = player_page.find("span", class_="nfl-c-player-header__position")

    # Player has no position and is not an NFL player
    if player_position is None:
        return False
    # Player is a QB
    if "QB" in player_position.text:
        return True
    # Player is not a QB
    else:
        return False
