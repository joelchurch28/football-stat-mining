from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import QMainWindow, QStackedWidget
from GUI.home_page import HomePage
from GUI.passing_page import PassingPage
import sys

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setGeometry(400, 400, 500, 500)
        self.setWindowTitle("Pass Stat Football")
        self.central_widget = QStackedWidget()
        self.setCentralWidget(self.central_widget)
        self.initUI()

    def initUI(self):
        self.home_page = HomePage(self)
        self.passing_page = PassingPage(self)

        self.central_widget.addWidget(self.home_page)
        self.central_widget.addWidget(self.passing_page)

    def set_page(self, index):
        self.central_widget.setCurrentIndex(index)


# main_window = MainWindow()