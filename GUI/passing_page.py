# filename: passing_page.py
# description: This file contains the implementation of Passing page in the application, where Passing is one of the
# four main pages of the application (home, passing, rushing, and receiving).
# author: Joel Church
# date: 2024-06-27
# version: 1.0.0

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *  # QApplication, QMainWindow, QWidget, QVBoxLayout, QLabel, QPushButton, QStackedWidget, QSpacerItem, QSizePolicy
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

from Players.quarterback_class import Quarterback
from SearchFunctions.passingfunctions import (currentYear, is_qb, first_page_passer_names, get_recent_passing_dict,
                                              get_career_passing_dict, get_year_passing_dict,
                                              search_career_passing_dict, qb_compare, all_qbs_since_1970,
                                              search_career_database_passing)
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

from GUI.singleton_main_window import SingletonMainWindow

import sys
import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="ds373737",
    database="FootballStats"
)

cursor = db.cursor()

teams = ["Cardinals",
         "Falcons",
         "Ravens",
         "Bills",
         "Panthers",
         "Bears",
         "Bengals",
         "Browns",
         "Cowboys",
         "Broncos",
         "Lions",
         "Packers",
         "Texans",
         "Colts",
         "Jaguars",
         "Chiefs",
         "Raiders",
         "Chargers",
         "Rams",
         "Dolphins",
         "Vikings",
         "Patriots",
         "Saints",
         "Giants",
         "Jets",
         "Eagles",
         "Steelers",
         "49ers",
         "Seahawks",
         "Buccaneers",
         "Titans",
         "Commanders"]

filter_keywords = {"None": "reg",
                   "Playoffs": "post",
                   "Home games": "home",
                   "Road games": "away",
                   "Wins": "wins",
                   "Losses": "losses"}


class PassingPage(QWidget):
    def __init__(self, parent=None):
        super(PassingPage, self).__init__(parent)
        self.initUI()
        self.installEventFilter(self)
        self.first_passer_filtered_results = None
        self.second_passer_filtered_results = None
        self.two_passers = False
        self.passer_search_popup = PopupSearch()
        # self.most_recent_submission = None

    def initUI(self):

        # Boolean values to make stats per game or include playoffs
        self.display_stats_per_game = False
        self.include_playoff_stats = False
        self.currently_displaying_stats_pergame = False

        # main_layouts
        self.stacked_layout = QStackedLayout()
        self.second_stacked_layout = QStackedLayout()
        main_layout = QVBoxLayout(self)
        header_layout = QHBoxLayout(self)
        num_passers_layout = QHBoxLayout(self)
        self.dropdown_layout = QHBoxLayout(self)
        dropdown_titles_layout = QHBoxLayout(self)
        dropdown_and_titles_layout = QVBoxLayout(self)
        #   Bottom section for stats, v-layouts within h-layout
        # names_above_stats_layout = QHBoxLayout(self)
        all_stats_layout = QHBoxLayout()
        stats_names_layout = QVBoxLayout()
        first_passer_stats_layout = QVBoxLayout()
        second_passer_stats_layout = QVBoxLayout()
        checkbox_layout = QVBoxLayout()

        # Back button and spacer
        self.back_button = QPushButton("back", self)
        self.back_button.clicked.connect(self.back_button_clicked)
        header_layout.addWidget(self.back_button, alignment=Qt.AlignTop)
        header_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # Title -- "Compare passing" -- and spacer
        # main_layout.addSpacerItem(QSpacerItem(20, 200, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.title = QLabel("Compare Passing", self)
        self.title.setFont(QFont("Arial", 30))
        self.title.setAlignment(Qt.AlignCenter)
        main_layout.addWidget(self.title)

        # Button for one and two passers
        self.one_passer_button = QPushButton()
        self.one_passer_button.setText("One passer")
        self.one_passer_button.clicked.connect(self.on_passer_button_clicked)
        self.two_passers_button = QPushButton()
        self.two_passers_button.setText("Two passers")
        self.two_passers_button.clicked.connect(self.on_passer_button_clicked)
        num_passers_layout.addWidget(self.one_passer_button)
        num_passers_layout.addWidget(self.two_passers_button)
        num_passers_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        main_layout.addLayout(num_passers_layout)

        # Titles for Dropdowns
        #   Passer dropdown title
        self.passer_dropdown_title = QLabel("  Passer", self)
        self.passer_dropdown_title.setFont(QFont("Arial", 17))
        self.passer_dropdown_title.setFixedSize(120, 20)
        dropdown_titles_layout.addWidget(self.passer_dropdown_title, alignment=Qt.AlignCenter)
        dropdown_titles_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        #   Conditional second Passer dropdown title
        self.second_passer_dropdown_title = QLabel("")
        self.second_passer_dropdown_title.setFixedSize(100, 20)
        self.second_passer_dropdown_title.setFont(QFont("Arial", 17))
        dropdown_titles_layout.addWidget(self.second_passer_dropdown_title, alignment=Qt.AlignCenter)
        dropdown_titles_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        #   Season dropdown title
        self.season_dropdown_title = QLabel("Season", self)
        self.season_dropdown_title.setFont(QFont("Arial", 17))
        dropdown_titles_layout.addWidget(self.season_dropdown_title, alignment=Qt.AlignCenter)
        dropdown_titles_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        #   Filter dropdown title
        self.filter_dropdown_title = QLabel("Filter", self)
        self.filter_dropdown_title.setFont(QFont("Arial", 17))
        dropdown_titles_layout.addWidget(self.filter_dropdown_title, alignment=Qt.AlignCenter)
        dropdown_titles_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        # Placeholder spacer above submit button
        dropdown_titles_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # Names above stats (e.g. "Dak Prescott" above his stats)
        # names_above_stats_layout.setSpacing(0)
        # names_above_stats_layout.addSpacerItem(QSpacerItem(250, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        # self.first_passer_name = QLabel("", self)
        # names_above_stats_layout.addWidget(self.first_passer_name)
        # self.second_passer_name = QLabel("", self)
        # names_above_stats_layout.addWidget(self.second_passer_name)
        # names_above_stats_layout.addSpacerItem(QSpacerItem(350, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # Passer Dropdown
        self.passer_dropdown = PasserDropdown()
        self.passer_dropdown.setEditable(True)
        # self.passer_dropdown.setFixedSize(150, 50)
        # self.passer_dropdown.setFont(QFont("Arial", 20))
        self.passer_dropdown.setPlaceholderText("Select passer")
        self.passer_dropdown.currentIndexChanged.connect(self.on_passer_selected)
        self.stacked_layout.addWidget(self.passer_dropdown)
        # self.dropdown_layout.addWidget(self.passer_dropdown, alignment= Qt.AlignCenter)
        self.dropdown_layout.addWidget(self.passer_dropdown)
        self.dropdown_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # conditional Second Passer Dropdown
        self.second_passer_dropdown = PasserDropdown()
        self.second_passer_dropdown.setEditable(True)
        # self.second_passer_dropdown.setFixedSize(150, 50)
        self.second_passer_dropdown.currentIndexChanged.connect(self.on_second_passer_selected)
        self.second_dropdown_placeholder = QLabel("")
        self.second_dropdown_placeholder.setFixedSize(170, 32)
        self.second_stacked_layout.addWidget(self.second_dropdown_placeholder)
        self.dropdown_layout.addLayout(self.second_stacked_layout)

        # Season Dropdown
        self.season_dropdown = SeasonDropdown()
        # self.season_dropdown.setFixedSize(150, 50)
        # self.season_dropdown.setFont(QFont("Arial", 20))
        self.dropdown_layout.addWidget(self.season_dropdown)
        self.season_dropdown.currentIndexChanged.connect(self.on_season_selected)
        self.dropdown_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # Filter Dropdown
        self.filter_dropdown = QComboBox()
        # self.filter_dropdown.setFixedSize(150, 50)
        self.filter_dropdown.setFont(QFont("Arial", 15))
        self.filter_dropdown.addItems(["None", "Playoffs", "Home games",
                                       "Road games", "Wins", "Losses", "vs. Team    >"])
        self.filter_dropdown.currentIndexChanged.connect(self.on_filter_selected)
        self.dropdown_layout.addWidget(self.filter_dropdown)
        self.dropdown_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))

        # Submit Button
        self.submit_button = QPushButton()
        self.submit_button.setText("submit")
        self.submit_button.clicked.connect(self.submit_button_clicked)
        self.dropdown_layout.addWidget(self.submit_button)

        # Names of statistics
        self.first_passer_name = QLabel("", self)
        stats_names_layout.addWidget(self.first_passer_name)
        self.record_name = QLabel("Record:", self)
        stats_names_layout.addWidget(self.record_name)
        self.pass_yards_name = QLabel("Pass yards:", self)
        stats_names_layout.addWidget(self.pass_yards_name)
        self.yards_att_name = QLabel("Yards/attempt:", self)
        stats_names_layout.addWidget(self.yards_att_name)
        self.attempts_name = QLabel("Attempts:", self)
        stats_names_layout.addWidget(self.attempts_name)
        self.completions_name = QLabel("Completions:", self)
        stats_names_layout.addWidget(self.completions_name)
        self.cmpt_pct_name = QLabel("Completion Percentage:", self)
        stats_names_layout.addWidget(self.cmpt_pct_name)
        self.touchdowns_name = QLabel("Touchdowns:", self)
        stats_names_layout.addWidget(self.touchdowns_name)
        self.interceptions_name = QLabel("Interceptions:", self)
        stats_names_layout.addWidget(self.interceptions_name)
        self.rating_name = QLabel("Passer Rating:", self)
        stats_names_layout.addWidget(self.rating_name)

        # Statistics themselves
        #   First passer stats
        self.first_passer_name = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_name)
        self.first_passer_record = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_record)
        self.first_passer_yards = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_yards)
        self.first_passer_yards_att = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_yards_att)
        self.first_passer_attempts = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_attempts)
        self.first_passer_completions = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_completions)
        self.first_passer_cmp_pct = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_cmp_pct)
        self.first_passer_touchdowns = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_touchdowns)
        self.first_passer_interceptions = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_interceptions)
        self.first_passer_rating = QLabel("", self)
        first_passer_stats_layout.addWidget(self.first_passer_rating)

        #   Second passer stats
        self.second_passer_name = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_name)
        self.second_passer_record = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_record)
        self.second_passer_yards = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_yards)
        self.second_passer_yards_att = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_yards_att)
        self.second_passer_attempts = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_attempts)
        self.second_passer_completions = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_completions)
        self.second_passer_cmp_pct = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_cmp_pct)
        self.second_passer_touchdowns = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_touchdowns)
        self.second_passer_interceptions = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_interceptions)
        self.second_passer_rating = QLabel("", self)
        second_passer_stats_layout.addWidget(self.second_passer_rating)
        # self.second_passer_firsts = QLabel("", self)
        # second_passer_stats_layout.addWidget(self.second_passer_firsts)
        # self.second_passer_first_pct = QLabel("", self)
        # second_passer_stats_layout.addWidget(self.second_passer_first_pct)

        # Checkboxes
        self.stats_per_game_checkbox = QCheckBox("Show stats per game", self)
        self.stats_per_game_checkbox.clicked.connect(self.on_stats_per_game_checkbox_clicked)
        self.stats_per_game_checkbox.setDisabled(True)
        checkbox_layout.addWidget(self.stats_per_game_checkbox)
        self.include_playoff_stats_checkbox = QCheckBox("Include playoff stats", self)
        self.include_playoff_stats_checkbox.clicked.connect(self.on_include_playoffs_checkbox_clicked)
        self.include_playoff_stats_checkbox.setDisabled(True)
        checkbox_layout.addWidget(self.include_playoff_stats_checkbox)

        self.setLayout(main_layout)
        main_layout.addSpacerItem(QSpacerItem(50, 400, QSizePolicy.Minimum, QSizePolicy.Expanding))
        dropdown_and_titles_layout.setSpacing(1)
        dropdown_and_titles_layout.addLayout(dropdown_titles_layout)
        dropdown_and_titles_layout.addLayout(self.dropdown_layout)
        main_layout.addLayout(dropdown_and_titles_layout)
        # main_layout.addLayout(names_above_stats_layout)
        all_stats_layout.addLayout(stats_names_layout)
        all_stats_layout.addLayout(first_passer_stats_layout)
        all_stats_layout.addSpacerItem(QSpacerItem(100, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        all_stats_layout.addLayout(second_passer_stats_layout)
        all_stats_layout.addLayout(checkbox_layout)
        all_stats_layout.addSpacerItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        main_layout.addLayout(all_stats_layout)
        main_layout.addSpacerItem(QSpacerItem(50, 400, QSizePolicy.Minimum, QSizePolicy.Expanding))

        # main_layout.addSpacerItem(QSpacerItem(50, 400, QSizePolicy.Minimum, QSizePolicy.Expanding))
        # main_layout.addSpacerItem(QSpacerItem(50, 400, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def on_passer_button_clicked(self):
        button_pressed = self.sender()
        button_pressed.setDown(True)

        self.disable_checkboxes()

        if button_pressed == self.one_passer_button and self.one_passer_button.isDown():
            print("one passer")
            self.two_passers = False
            self.two_passers_button.setDown(False)
            if len(self.second_passer_attempts.text()) > 0:
                self.clear_second_passer_stats()
                if len(self.first_passer_attempts.text()) > 0:
                    self.update_names_above_stats()
            self.second_passer_dropdown_title.setText("")
            self.passer_dropdown_title.setText("  Passer")
            self.second_stacked_layout.removeWidget(self.second_passer_dropdown)
            self.second_stacked_layout.addWidget(self.second_dropdown_placeholder)
        elif button_pressed == self.two_passers_button and self.two_passers_button.isDown():
            print("two passers")
            self.two_passers = True
            self.one_passer_button.setDown(False)
            self.passer_dropdown_title.setText("  Passer 1")
            self.second_passer_dropdown_title.setText("Passer 2")
            self.second_stacked_layout.removeWidget(self.second_dropdown_placeholder)
            self.second_stacked_layout.addWidget(self.second_passer_dropdown)

    def disable_checkboxes(self):
        """ Disables all checkboxes

        :return: None
        """
        self.include_playoff_stats_checkbox.setChecked(False)
        self.stats_per_game_checkbox.setDisabled(False)
        self.include_playoff_stats = False
        self.display_stats_per_game = False
        self.include_playoff_stats_checkbox.setDisabled(True)
        self.stats_per_game_checkbox.setDisabled(True)

    def enable_checkboxes(self):
        """Enables all checkboxes

        :return: None
        """

        self.include_playoff_stats_checkbox.setDisabled(False)
        self.stats_per_game_checkbox.setDisabled(False)

    def on_season_selected(self):
        if self.display_stats_per_game:
            self.stats_per_game_checkbox.click()

        self.disable_checkboxes()

    def on_filter_selected(self, index):
        """Is called every time a choice is selected from the filter dropdown menu. Shows an expanded side menu if
        a team or player is chosen for comparison

        :param index: The index of the selected choice in the 'filter' dropdown menu -> int
        :return: none
        """
        if self.display_stats_per_game:
            self.stats_per_game_checkbox.click()

        self.disable_checkboxes()

        if self.filter_dropdown.itemText(index) == "vs. Team    >":
            self.show_team_menu()

    def show_team_menu(self):
        """Displays the pop-up menu containing all 32 NFL teams, all of which are clickable. Clicking a team displays
        the team name in the "filter" dropdown menu

        :return: none
        """
        team_menu = QMenu(self)

        for team in teams:
            action = QAction(team, self)
            action.triggered.connect(lambda checked, t=team: self.on_team_selected(t))
            team_menu.addAction(action)

        team_menu.exec_(self.mapToGlobal(self.filter_dropdown.pos()))

    def on_team_selected(self, team):
        """ Adds the team name to the "filter" dropdown menu when it is selected and displays it. If the team is already
        in the dropdown menu, it still displays it as the current text, but does not add it again.

        :param team: The team selected -> str
        :return: none
        """
        if self.filter_dropdown.findText(team) == -1:
            self.filter_dropdown.addItem(team)

        index = self.filter_dropdown.findText(team)
        if index != -1:
            self.filter_dropdown.setCurrentIndex(index)
        else:
            self.filter_dropdown.setCurrentText(team)

    # def on_vs_passer_selected(self, passer):
    #     """Adds the passer's name to the "filter" dropdown menu when it is selected and displays it. If the passer's
    #     name is already in the dropdown menu, it still displays it as the current text, but does not add it again.
    #
    #     :param passer: The passer name selected -> str
    #     :return: none
    #     """
    #     if self.filter_dropdown.findText(passer) == -1:
    #         self.filter_dropdown.addItem(passer)
    #
    #     index = self.filter_dropdown.findText(passer)
    #     if index != -1:
    #         self.filter_dropdown.setCurrentIndex(index)
    #     else:
    #         self.filter_dropdown.setCurrentText(passer)

    def on_passer_selected(self, index):
        """Displays the passer name selected from the "passer" dropdown menu as its current text

        :param passer: The passer name selected -> str
        :return: none
        """
        if self.display_stats_per_game:
            self.stats_per_game_checkbox.click()

        self.disable_checkboxes()

        passer_name = self.passer_dropdown.currentText()


    def on_second_passer_selected(self, index):
        """Displays the passer name selected from the second "passer" dropdown menu as its current text

        :param passer: The passer name selected -> str
        return: none
        """
        if self.display_stats_per_game:
            self.stats_per_game_checkbox.click()

        self.disable_checkboxes()

        passer_name = self.second_passer_dropdown.currentText()


    def back_button_clicked(self):
        """Returns to the home page

        :return: none
        """
        main_window = SingletonMainWindow.get_instance()
        main_window.set_page(0)

    def on_stats_per_game_checkbox_clicked(self):
        """ Switches the boolean display_stats_per_game from its current state and updates the displayed stats, if
        there are any, accordingly.

        :return: none
        """
        print(f"Checkbox clicked. Changed from {self.display_stats_per_game} to {not self.display_stats_per_game}.")
        self.display_stats_per_game = not self.display_stats_per_game

        if len(self.first_passer_rating.text()) > 1:
            self.update_first_passer_stats()
            self.update_second_passer_stats()

    def on_include_playoffs_checkbox_clicked(self):
        """Switches the boolean include_playoff_stats from its current state and updates the displayed stats, if
        there are any, accordingly.

        :return:
        """
        self.include_playoff_stats = not self.include_playoff_stats

        if len(self.first_passer_rating.text()) > 1:
            self.submit_button_clicked()
            self.submit_button_clicked()


    def submit_button_clicked(self):
        """Handles a number of circumstances upon the clicking of the "subimt" button. If the submit button is clicked
        while either checkbox is checked, it becomes unchecked before anything happens. Updates player stats according
        to the values currently selected in the three dropdown menus: "player", "season", and "filter"

        :return: none
        """
        print("Stats per game", self.display_stats_per_game)


        if self.filter_dropdown.currentText() != "Playoffs":
            self.enable_checkboxes()
        else:
            self.stats_per_game_checkbox.setDisabled(False)

        if self.first_passer_filtered_results is None:
            self.first_passer_filtered_results = [Quarterback(), 0, 0, 0]
        if self.first_passer_filtered_results is None:
            self.second_passer_filtered_results = [Quarterback(), 0, 0, 0]

        if self.display_stats_per_game:
            self.stats_per_game_checkbox.click()
            self.display_stats_per_game = False

        self.update_names_above_stats()

        # Update stats
        # If the filter isn't a player
        if not self.two_passers:
            self.second_passer_name.clear()
            self.first_passer_career = search_career_database_passing(cursor, db, self.passer_dropdown.currentText(),
                                                                 self.filter_dropdown.currentText(),
                                                                 self.season_dropdown.currentText()) if not self.include_playoff_stats \
                else search_career_database_passing(cursor, db, self.passer_dropdown.currentText(),
                                                    self.filter_dropdown.currentText(),
                                                    self.season_dropdown.currentText(), True)
            self.update_first_passer_stats(self.first_passer_career)
            self.clear_second_passer_stats()
        # The filter is a player
        else:
            self.first_passer_career = search_career_database_passing(cursor, db, self.passer_dropdown.currentText(),
                                                                 self.filter_dropdown.currentText(),
                                                                 self.season_dropdown.currentText()) if not self.include_playoff_stats \
                else search_career_database_passing(cursor, db, self.passer_dropdown.currentText(),
                                                    self.filter_dropdown.currentText(),
                                                    self.season_dropdown.currentText(), True)
            self.second_passer_career = search_career_database_passing(cursor, db, self.second_passer_dropdown.currentText(),
                                                                 self.filter_dropdown.currentText(),
                                                                 self.season_dropdown.currentText()) if not self.include_playoff_stats \
                else search_career_database_passing(cursor, db, self.second_passer_dropdown.currentText(),
                                                    self.filter_dropdown.currentText(),
                                                    self.season_dropdown.currentText(), True)
            self.update_first_passer_stats(self.first_passer_career)
            self.update_second_passer_stats(self.second_passer_career)

    def update_names_above_stats(self):
        # First update the names above the stats, e.g. "Trevor Lawrence - Home games - 2022", etc.
        # If the filter isn't vs. a team and only one qb is being analyzed
        if not self.two_passers and self.filter_dropdown.currentText() not in teams:
            # Deploy {name} - {filter} - {season} format
            self.first_passer_name.setText(f"{self.passer_dropdown.currentText()} -"
                                           f" {self.filter_dropdown.currentText()} -"
                                           f" {self.season_dropdown.currentText()}") \
                if self.filter_dropdown.currentText() != "None" else (
                self.first_passer_name.setText(f"{self.passer_dropdown.currentText()} -"
                                               f" {self.season_dropdown.currentText()}"))
            self.first_passer_name.update()
            self.second_passer_name.setText("")
        # If the filter is vs. a team
        elif self.filter_dropdown.currentText() in teams:
            # Deploy {name} vs. {team name} format
            self.first_passer_name.setText(f"{self.passer_dropdown.currentText()} -"
                                           f" vs. {self.filter_dropdown.currentText()} -"
                                           f" {self.season_dropdown.currentText()}")
            self.first_passer_name.update()
        # If two players are being analyzed
        elif self.two_passers:
            # Place each player's name over their respective statistics
            self.first_passer_name.setText(f"{self.passer_dropdown.currentText()}            ")
            self.second_passer_name.setText(f"{self.second_passer_dropdown.currentText()}")

    def update_first_passer_stats(self, passing_list="none"):
        """Updates the displayed stats of the first player. If the function receives a passing dictionary, new stats
        take the place of old ones (or lack thereof). If the function receives no parameter, there are already stats on
        the screen, and they will be converted to or from average stats per game, depending on their current state.

        :param passing_list: a list with a list representing a statline with the following values: completions, pass attempts,
        completion percentage, pass yards, pass td, interceptions, passer rating, times sacked, sack yards, pass yards/att,
        rush attempts, rush yards, rush yards/att, rush td, fumbles, and fumbles lost. After this list there are three
        integers, one representing wins, one for losses, and ties
        :return: none
        """

        if passing_list != "none":
            # Display NEW stats without converting to per game
            # self.set_stats_titles("default")
            self.first_passer_record.setText(
                f"{passing_list[1]} - {passing_list[2]} - {passing_list[3]}")
            self.first_passer_yards.setText(str(passing_list[0][3]))
            self.first_passer_yards_att.setText(str(passing_list[0][9]))
            self.first_passer_attempts.setText(str(passing_list[0][1]))
            self.first_passer_completions.setText(str(passing_list[0][0]))
            self.first_passer_cmp_pct.setText(str(passing_list[0][2]))
            self.first_passer_touchdowns.setText(str(passing_list[0][4]))
            self.first_passer_interceptions.setText(str(passing_list[0][5]))
            self.first_passer_rating.setText(str(passing_list[0][6]))

        # If this function was called without a dictionary and there are already stats on the screen
        elif len(self.first_passer_rating.text()) > 1:
            # Convert default stats to per game
            if self.display_stats_per_game:
                # self.set_stats_titles("pergame")
                first_qb_wlt = self.first_passer_record.text().split(' - ')
                first_qb_games_played = int(first_qb_wlt[0]) + int(first_qb_wlt[1]) + int(first_qb_wlt[2])
                self.first_passer_yards.setText(
                    str(round(float(self.first_passer_yards.text()) / first_qb_games_played, 1))) if (
                        float(self.first_passer_yards.text()) != 0.0) else 0
                self.first_passer_attempts.setText(
                    str(round(float(self.first_passer_attempts.text()) / first_qb_games_played, 1))) if (
                        float(self.first_passer_attempts.text()) != 0.0) else 0
                self.first_passer_completions.setText(
                    str(round(float(self.first_passer_completions.text()) / first_qb_games_played, 1))) if (
                        float(self.first_passer_completions.text()) != 0.0) else 0
                self.first_passer_touchdowns.setText(
                    str(round(float(self.first_passer_touchdowns.text()) / first_qb_games_played, 1))) if (
                        float(self.first_passer_touchdowns.text()) != 0.0) else 0
                self.first_passer_interceptions.setText(
                    str(round(float(self.first_passer_interceptions.text()) / first_qb_games_played, 1))) if (
                        float(self.first_passer_interceptions.text()) != 0.0) else 0
            # Convert per game stats to default
            else:
                # self.set_stats_titles("default")
                self.first_passer_yards.setText(str(self.first_passer_career[0][3]))
                self.first_passer_attempts.setText(str(self.first_passer_career[0][1]))
                self.first_passer_completions.setText(str(self.first_passer_career[0][0]))
                self.first_passer_touchdowns.setText(str(self.first_passer_career[0][4]))
                self.first_passer_interceptions.setText(str(self.first_passer_career[0][5]))

        # If there are no stats on the screen and there is no dictionary, do nothing
        else:
            pass

    def update_second_passer_stats(self, passing_list="none"):
        """Updates the displayed stats of the second player. If the function receives a passing dictionary, new stats
        take the place of old ones (or lack thereof). If the function receives no parameter, there are already stats on
        the screen, and they will be converted to or from average stats per game, depending on their current state.

        :param passing_list: a list with a list representing a statline with the following values: completions, pass attempts,
        completion percentage, pass yards, pass td, interceptions, passer rating, times sacked, sack yards, pass yards/att,
        rush attempts, rush yards, rush yards/att, rush td, fumbles, and fumbles lost. After this list there are three
        integers, one representing wins, one for losses, and ties
        :return: none
        """

        if passing_list != "none":
            # Display NEW stats without converting to per game
            # self.set_stats_titles("default")
            self.second_passer_record.setText(
                f"{passing_list[1]} - {passing_list[2]} - {passing_list[3]}")
            self.second_passer_yards.setText(str(passing_list[0][3]))
            self.second_passer_yards_att.setText(str(passing_list[0][9]))
            self.second_passer_attempts.setText(str(passing_list[0][1]))
            self.second_passer_completions.setText(str(passing_list[0][0]))
            self.second_passer_cmp_pct.setText(str(passing_list[0][2]))
            self.second_passer_touchdowns.setText(str(passing_list[0][4]))
            self.second_passer_interceptions.setText(str(passing_list[0][5]))
            self.second_passer_rating.setText(str(passing_list[0][6]))

        # If this function was called without a dictionary and there are already stats on the screen
        elif len(self.second_passer_rating.text()) > 1:
            # Convert default stats to per game
            if self.display_stats_per_game:
                # self.set_stats_titles("pergame")
                second_qb_wlt = self.second_passer_record.text().split(' - ')
                second_qb_games_played = int(second_qb_wlt[0]) + int(second_qb_wlt[1]) + int(second_qb_wlt[2])
                self.second_passer_yards.setText(
                    str(round(float(self.second_passer_yards.text()) / second_qb_games_played, 1))) if (
                        int(self.second_passer_yards.text()) != 0) else 0
                self.second_passer_attempts.setText(
                    str(round(float(self.second_passer_attempts.text()) / second_qb_games_played, 1))) if (
                        int(self.second_passer_attempts.text()) != 0) else 0
                self.second_passer_completions.setText(
                    str(round(float(self.second_passer_completions.text()) / second_qb_games_played, 1))) if (
                        int(self.second_passer_completions.text()) != 0) else 0
                self.second_passer_touchdowns.setText(
                    str(round(float(self.second_passer_touchdowns.text()) / second_qb_games_played, 1))) if (
                        int(self.second_passer_touchdowns.text()) != 0) else 0
                self.second_passer_interceptions.setText(
                    str(round(float(self.second_passer_interceptions.text()) / second_qb_games_played, 1))) if (
                        int(self.second_passer_interceptions.text()) != 0) else 0
            # Convert per game stats to default
            else:
                # self.set_stats_titles("default")
                self.second_passer_yards.setText(str(self.second_passer_career[0][3]))
                self.second_passer_attempts.setText(str(self.second_passer_career[0][1]))
                self.second_passer_completions.setText(str(self.second_passer_career[0][0]))
                self.second_passer_touchdowns.setText(str(self.second_passer_career[0][4]))
                self.second_passer_interceptions.setText(str(self.second_passer_career[0][5]))

        # If there are no stats on the screen and there is no dictionary, do nothing
        else:
            pass

    def clear_second_passer_stats(self):
        """Clears the values of all the second passer's stats.

        :return: none
        """
        self.second_passer_record.clear()
        self.second_passer_yards.clear()
        self.second_passer_yards_att.clear()
        self.second_passer_attempts.clear()
        self.second_passer_completions.clear()
        self.second_passer_cmp_pct.clear()
        self.second_passer_touchdowns.clear()
        self.second_passer_interceptions.clear()
        self.second_passer_rating.clear()

    def disable_all_fields(self):
        """Disables all non-checkbox clickable fields

        :return:
        """
        self.passer_dropdown.setDisabled(True)
        self.season_dropdown.setDisabled(True)
        self.filter_dropdown.setDisabled(True)
        self.submit_button.setDisabled(True)

    def enable_all_fields(self):
        """Enables all non-checkbox clickable fields

        :return:
        """
        self.passer_dropdown.setDisabled(False)
        self.season_dropdown.setDisabled(False)
        self.filter_dropdown.setDisabled(False)
        self.submit_button.setDisabled(False)


class PasserDropdown(QComboBox):
    def __init__(self):
        super().__init__()
        self.resize(500, 500)
        self.setFixedSize(170, 32)
        self.setStyleSheet('font-size: 15px;')
        self.qb_names_to_check = all_qbs_since_1970()

        for name in self.qb_names_to_check:
            self.addItem(name)

        self.setCurrentIndex(0)


class SeasonDropdown(QComboBox):
    def __init__(self):
        super().__init__()
        self.resize(500, 500)
        self.setStyleSheet('font-size: 15px;')

        self.addItem("career")

        for year in range(int(currentYear), 1969, -1):
            self.addItem(str(year))


class PopupSearch(QWidget):
    def __init__(self):
        super().__init__()

        self.setGeometry(10, 10, 100, 100)

        main_layout = QHBoxLayout()

        # ok button
        self.ok_button = QPushButton("OK", self)
        self.ok_button.setFixedSize(50, 30)

        # search box
        self.search_box = QLineEdit()
        self.search_box.setPlaceholderText("search...")
        self.search_box.setFixedSize(75, 30)
        self.search_box.returnPressed.connect(self.ok_button.click)

        main_layout.addWidget(self.search_box, alignment=Qt.AlignCenter)
        main_layout.addWidget(self.ok_button, alignment=Qt.AlignCenter)

        self.setLayout(main_layout)
