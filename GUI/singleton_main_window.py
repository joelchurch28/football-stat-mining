
class SingletonMainWindow:
    _instance = None

    @staticmethod
    def get_instance():
        if SingletonMainWindow._instance is None:
            from GUI.main_window import MainWindow
            SingletonMainWindow._instance = MainWindow()
        return SingletonMainWindow._instance