# filename: home_page.py
# description: This file contains the implementation of Home page in the application, where Passing is one of the
# four main pages of the application (home, passing, rushing, and receiving).
# author: Joel Church
# date: 2024-06-27
# version: 1.0.0
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import * #QApplication, QMainWindow, QWidget, QVBoxLayout, QLabel, QPushButton, QStackedWidget, QSpacerItem, QSizePolicy
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

from GUI.singleton_main_window import SingletonMainWindow

import sys

class HomePage(QWidget):
    def __init__(self, parent=None):
        super(HomePage, self).__init__(parent)
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout(self)

        layout.addSpacerItem(QSpacerItem(20, 200, QSizePolicy.Minimum, QSizePolicy.Expanding))  # Top spacer

        self.title = QLabel("Pass Stat Football", self)
        self.title.setFont(QFont("Arial", 50))
        self.title.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.title)

        self.subtitle = QLabel("Analyze and compare football stats")
        self.subtitle.setFont(QFont("Arial", 20))
        self.subtitle.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.subtitle)

        self.three_buttons_layout = QVBoxLayout()

        self.passing_button = QPushButton("Passing", self)
        self.passing_button.setFixedSize(175, 50)
        self.passing_button.clicked.connect(self.passing_button_clicked)
        self.three_buttons_layout.addWidget(self.passing_button, alignment=Qt.AlignCenter)

        self.rushing_button = QPushButton("Rushing", self)
        self.rushing_button.setFixedSize(175, 50)
        self.rushing_button.clicked.connect(self.rushing_button_clicked)
        self.three_buttons_layout.addWidget(self.rushing_button, alignment=Qt.AlignCenter)

        self.receiving_button = QPushButton("Receiving", self)
        self.receiving_button.setFixedSize(175, 50)
        self.receiving_button.clicked.connect(self.receiving_button_clicked)
        self.three_buttons_layout.addWidget(self.receiving_button, alignment=Qt.AlignCenter)

        layout.addSpacerItem(QSpacerItem(20, 200, QSizePolicy.Minimum, QSizePolicy.Expanding))  # Top spacer
        layout.addLayout(self.three_buttons_layout)
        layout.addSpacerItem(QSpacerItem(20, 200, QSizePolicy.Minimum, QSizePolicy.Expanding))  # Top spacer

        self.setLayout(layout)

    def passing_button_clicked(self):
        main_window = SingletonMainWindow.get_instance()
        main_window.set_page(1)

    def rushing_button_clicked(self):
        not_implemented_box = QtWidgets.QMessageBox(self)
        not_implemented_box.setText("Not yet implemented!")
        not_implemented_box.exec_()

    def receiving_button_clicked(self):
        not_implemented_box = QtWidgets.QMessageBox(self)
        not_implemented_box.setText("Not yet implemented!")
        not_implemented_box.exec_()
